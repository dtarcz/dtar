<?php 

require_once("/home/dtarfrdfjf/www/crawlprotect/include/cppf.php"); 

$crawltsite=1;
require_once("/home/dtarfrdfjf/www/crawltrack/crawltrack.php");

?>
<!DOCTYPE html>
	<head>
		<meta charset="utf-8" />
		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<title>Accueil</title>
		<meta name="description" content="Page d'accueil du site de David" />
		<meta name="keywords" content="Reiki, Décroissance, Jeux de Rôle, Jeu de GO" />
		<link rel="stylesheet" media="screen" type="text/css" title="dtar01" href="style_01.css" />
	</head>
	
	<!-- DEBUT SCRIT GOOGLE ANALYTIC -->
	
   	<!-- FIN SCRIT GOOGLE ANALYTIC -->
   		
	<body>
   
		<header>
			<?php // Inclusion du bandeau:
			include("haut.php"); 
			?>
		</header>
 
		<nav>
			<?php // Inclusion du menu:
			include("menu.php"); 
			?>			
		</nav>
 
		<section>

			<h1>Site de David</h1>
			<p>Bienvenue sur ce <em>modeste site</em> pour <strong>l'Amour, la Paix, la Joie...</strong>.</p>
			<p>Merci <strong>à toi</strong> de le visiter.</p>
			<p>Tu trouveras ici ce qui me tiens à coeur :</p>
		
				<h2>Le Jeu</h2>
				<p>S'amuser, c'est aussi une excellente façon de faire connaissance :)</p>
					<h3>Le jeu de rôle</h3>
					<h3>le jeu de GO</h3>
					<h3>Le jeu en ligne</h3>
					<p>Exclusivement des jeux Libres et Gratuits, multi-joueurs, pour passer de bons moments dans la convivialité.</p>
						<h4>Globulation2</h4>
						<p><img src="img/glob2-icon-48x48.png" class="imageflottante" alt="Image flottante" /> Aaah, voilà un jeu absolument génial ! Globulation 2 est un jeu de Stratégie Temps Réel (RTS Real-Time Strategy) innovant qui réduit la gestion du joueur en assignant automatiquement des tâches aux unités. Et bien que ce soit aussi un jeu de conquête, il est complètement adapté aux plus jeunes, je trouve.</p>
						<h4>Landes Éternelles</h4>
						<p><img src="img/icon-le.bmp" class="imageflottante" alt="Image flottante" /> Un jeu véritablement fascinant en 3D. On y incarne un personnage qui évolue au gré de nos envies... Carrément du jeu de rôle avec support graphique.</p>
						<h4>The Mana World</h4>
						<p><img src="img/Tmw_logo-mini.png" class="imageflottante" alt="Image flottante" /> Un petit jeu très mignon en 2D, sur lequel on peut passer des heures, là aussi pour faire évoluer un personnage.</p>
						<h4>Open TTD</h4>
						<p><img src="img/openttd.32.bmp" class="imageflottante" alt="Image flottante" /> On change de registre, pour construire un réseau de transport. Chemin de fer, bateaux, et transport routier, serez-vous capable de faire prospérer votre petite société ? Vous pourrez même faire grandir des villes, créer des industries, et construire des gares et des réseaux de transports hallucinants.</p>
						<h4>FreeCiv</h4>
						<p><img src="img/civicon.png" class="imageflottante" alt="Image flottante" /> Vous démarrez avec un petit colon et un explorateur, pour créer une civilisation capable d'aller dans les étoiles... Avec FreeCiv, vous faites évoluer une civilisation de 4000 ans avant J.C. jusque dans le futur.</p>
						<h4>FreeCol</h4>
						<p><img src="img/freecol-mini.png" class="imageflottante" alt="Image flottante" /> Petit frère de FreeCiv, sur le continent des Amériques. Vous incarnez une civilisation européenne en 1360 qui découvre et colonise le nouveau monde, pour finalement s'émanciper de sa métropole.</p>
						
				<h2>La Décroissance</h2>
				<p>Des idées pour vivre en harmonie et dans le <strong>respect de la planète, des animaux, des autres et de soi-même</strong>.</p>
				<p><img src="img/velo.png" class="imageflottante" alt="Image flottante" /> Voici un vélo, qui représente pour moi à l'heure actuelle la meilleure façon de se déplacer. On profite du bon air, et on ne polue pas. Argument non négligeable, c'est très économique comme mode de déplacement !</p>
				
							
				<h2>Le Reïki</h2>
				<p>Tu vas découvrir mon activité <strong>Reïki</strong></p>
								
		
		</section>
 
		<footer>
			<?php
			include("pied.php"); 
			?>
		</footer>
		
	<!-- phpmyvisites -->
	
	<!-- /phpmyvisites -->
	       
	   </body>
   
</html>
