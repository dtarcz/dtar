<?xml version='1.0' encoding='UTF-8'?>
<document>
	<title><![CDATA[Inkscape : Dessinez un avatar feminin style BD]]></title>
	<allow_com>1</allow_com>
	<template><![CDATA[article.php]]></template>
	<chapo><![CDATA[<p>Un <strong>tutoriel Inkscape</strong> : En quelques étapes simples dessinez un visage féminin de dessin animé. Il vous servira d'avatar, ou juste d'exercice...</p>]]></chapo>
	<content><![CDATA[<p>Aujourd'hui, grâce à ma petite sœur, vous avez droit à la traduction du tutoriel de Olga Bikmullina.</p>
<p>En effet, elle ne parle pas encore assez bien anglais pour le suivre, j'ai donc demandé l'autorisation à Olga Bukmullina de traduire son ombre-image, qui me l'a accordé avec joie.</p>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/title_en-619x403.png" alt="avatar féminin titre anglais" width="619" height="403" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />L'image originale du
    <br /><a href="http://is.gd/hhCV4A">tutoriel de Olga Bikmullina</a></figcaption>
</figure>
<p>Un avatar est la représentation graphique de l'utilisateur dans les réseaux sociaux, forums, jeux, etc. En d'autres termes, il est son alter ego.
<br />Habituellement un avatar 2D est une image carrée qui a une petite taille (100x100px, 64x64px). C'est pourquoi nous devrions supprimer tous les éléments indésirables et garder seulement les plus importants lorsque nous le créons. Sinon, il serait soit difficile de voir les détails après réduction de la taille, soit ça rendrait une image floue.</p>
<p>Il y a plusieurs fonctionnalités utiles dans <strong>Inkscape</strong> qui peuvent vous aider à créer facilement un avatar:</p>
<ul>
<li><p><strong>Affichage &gt; Aperçu d'icône</strong> vous permettra de prévisualiser la page ou les objets sélectionnés comme une icône dans différentes résolutions (16x16px / 24x24px / 32x32px / 48x48px et 128x128px).</p></li>
<li><p><strong>Voir &gt; Dupliquer la fenêtre</strong> va ouvrir une nouvelle fenêtre avec le même document dans lequel vous travaillez. En modifiant le document original, les modifications seront apportées dans ce duplicata. En plus, vous pouvez faire des changements dans les deux fenêtres. Cette fonctionnalité peut être utile lorsque vous travaillez dans <strong>Inkscape</strong> en règle générale, et pas seulement pour l'exercice d'aujourd'hui...</p></li>
</ul>
<p>Aujourd'hui, observons le processus de création d'un avatar féminin au style "bande dessinée" avec Inkscape.</p>
<h2>Et nous commencerons avec des objets simples comme des cercles, des ellipses, des rectangles et des carrés.</h2>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/1.png" alt="inscape avatar féminin image1 anglais" width="620" height="496" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Les forme simples : des cercles, des ellipses, des rectangles et des carrés,
<br>transformées en chemin puis retouchées avec l'outil nœud (F2)</figcaption>
</figure>
<h3>Dessinez</h3>
<p>
<ol>
<li>Faites un cercle avec l'outil "Créer des cercles, des ellipses et des arcs" (F5)</li>
<li>Convertissez ce cercle en chemin : (Chemin>Objet en chemin, ou [Maj+Ctrl+C])</li>
<li>Modifiez le chemin obtenu avec l'outil "Éditer les nœuds" (F2) pour le faire ressembler à une forme de visage humain</li>
<li>De même créez une ellipse, transformez-là en chemin et éditez ses nœuds, afin que l'objet ressemble à une oreille. Dupliquez l'oreille (Ctrl+D) et retournez-la horizontalement (Objet&gt;Retourner horizontalement les objets (H)). Placez les deux oreilles de part et d'autre au niveau du milieu de notre visage.</li>
<li>Pour créer le cou,  dessinez un rectangle (F4), convertissez-le en chemin et déplacez les deux nœuds inférieurs plus près l'un de l'autre. Pour ce faire sélectionnez-les (F2+cliquer-déplacer) et appuyez sur Ctrl+&lt; à plusieurs reprises.</li>
<li>Créez les épaules avec l'outil Rectangle (F4) et faites les nœuds du haut doux (Ctrl+Maj+C, F2, cliquez-glissez, "Rendre doux les nœuds sélectionnés"). Les épaules ont intentionnellement une petite taille de sorte que l'attention de l'observateur soit fixée sur le visage.</li>
</ol>
</p>
<h3>Mettez la couleur</h3>
<p>Il est assez difficile de trouver le bon ton de peau pour votre personnage, donc j'utilise une petite aide : le "<a href="http://is.gd/iRNQDa" >Catalogue des caractéristiques humaines</a>" réalisé par majnouna. Vous pouvez aussi utiliser ce document pour choisir de jolies couleurs d'yeux et de cheveux.</p>
<h3>Ajoutez du volume</h3>
<p>Pour séparer visuellement le visage du cou, vous devez ajouter une ombre sous lui.<br />
Pour ce faire, dupliquez le cou et remplissez-le avec une teinte de peau foncée. Dupliquez le visage et faites-le descendre un tout petit peu en maintenant touche Ctrl enfoncée, ou avec la flèche "bas". Ensuite, sélectionnez le cou assombri, le visage dupliqué et faites une intersection: Chemin&gt;Intersection (Ctrl+*).</p>
<h2>Continuons avec quelques détails</h2>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/2.png" alt="avatar féminin image2 inkscape" width="620" height="496" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Les détails du visage,
<br>Ellipses (E ou F5) et courbes de Bézier (Maj+F6) retouchées avec l'outil nœud (F2)</figcaption>
</figure>
<h3>Dessinez</h3>
<p>
<ol>
<li>Habituellement, les yeux sont en forme d'amande et se situent au même niveau que les bords supérieurs des oreilles. Créez une ellipse (F5) transformez-la en chemin (Maj+Ctrl+C) et resserrez ses bords en éditant les nœuds (F2). Créez deux cercles concentriques pour l'iris et la pupille.</li>
<li>Dessinez un sourcil avec l'outil Courbes de Bézier (Maj+F6) avec le style de forme "Triangle décroissant". Convertissez l'objet en chemin (Ctrl+Maj+C) et lissez les nœuds qui le nécessitent.
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/inkscape-courbes-bezier-forme-triangle-decroissant.png" alt="Inkscape courbes de Bézier forme triangle décroissant" width="400" height="210" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />lissez les nœuds qui le nécessitent.</figcaption>
</figure></li>
<li>Tracez une ligne de cils toujours avec l'outil "Courbes de Bézier" (Maj + F6) mais cette fois avec "Ellipse" en style de forme. Deux cils séparés peuvent facilement être effectués encore avec "Courbes de Bézier" et "Triangle décroissant" comme style de forme.</li>
<li>Ajoutez des détails aux oreilles en utilisant "Courbes de Bézier" (Maj + F6) en style de forme "Ellipse". Convertissez les deux objets en chemin et donnez-leur la forme indiquée. Après cela, vous pouvez unir ces deux chemins avec Chemin&gt;Union (Ctrl++).</li>
<blockquote><strong>Remarque №1:</strong> Après la conversion de la Courbe de Bézier créée en forme "Ellipse" en chemin, il peut y avoir un grand nombre de nœuds inutiles. Pour réduire leur nombre allez dans le menu  Chemin&gt;Simplifier ou appuyez simplement sur Ctrl+L le chemin étant bien sûr sélectionné.</blockquote>
<blockquote><strong>Remarque №2:</strong> Vous aurez noté que de nombreux éléments de la tête sont symétriques et que cela rend notre tâche beaucoup plus facile: Dans ce cas, nous pouvons créer un seul œil, le dupliquer (Ctrl+D) et le basculer horizontalement (H), tout comme pour les oreilles et leurs détail.
</blockquote>
<li>Dessinez des objets pour la bouche, le nez et les cheveux comme indiqué à l'image 2 (Pic.2) (avec l'outil "Courbes de Bézier"). Donnez-leur la forme désirée avec l'outil d'édition des nœuds (F2).</li>
<li>Très probablement, nous aurons besoin de descendre les cheveux à l'arrière-plan (Objet&gt;Descendre à l'arrière-plan ou Fin).</li>
<li>Dessinez un rectangle avec une échancrure par-dessus les épaules de l'avatar (avec l'outil Courbes de Bézier). Dupliquez les épaules, sélectionnez-les avec le rectangle et aller au menu Chemin&gt;Intersection (Ctrl+*).</li>
</ol>
</p>
<h3>Mettez la couleur</h3>
<p>Vous pouvez choisir des couleurs par vous-même ou vous pouvez les obtenir à partir du <a href="http://is.gd/rHMkeW" >fichier source / image</a> en utilisant le sélecteur de couleur (la pipette d'Inkscape) (F7).</p>
<h2>Améliorons les détails</h2>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/3.png" alt="Inkscape avatar féminin 3" width="620" height="496" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Ajouts d'ombres et de reflets.</figcaption>
</figure></li>
<h3>Ajoutez du volume</h3>
<p>
<ol>
<li>Le reflet de la lumière dans les yeux est souvent représenté par une petite bille blanche. Ajoutez un cercle blanc à la pupille et un demi-cercle vert plus clair à l'iris. Faites ressortir les yeux un peu plus en ajoutant une ombre en dessous: créez un objet avec l'outil Courbes de Bézier (Maj+F6) et faites lui les nœuds doux.
<blockquote><strong>Remarque:</strong> Malgré le fait que les yeux sont mis en miroir (retournés horizontalement), la position de la pupille et son reflet de lumière reste la même.</blockquote></li>
<li>Le volume des cheveux peut être ajouté à l'aide d'éclaircissements et d'ombres: la partie sur laquelle tombe la source de lumière sera plus claire. Créez deux boucles de cheveux et une ombre pour les cheveux derrière le cou avec l'outil Courbes de Bézier (Maj+F6).</li>
<li>Le volume des lèvres peut être ajouté avec de petits objets qui rappellent leur contour. Après lissage des nœuds, remplissez-les avec une couleur plus claire et ajoutez des reflets blancs (un cercle pour la lèvre du haut et une ellipse pour celle du bas).</li>
<li>Créez l'ombre des cheveux sur le visage et les ombres de bras sur le cardigan avec l'outil Bézier (Maj+F6).</li>
</ol>
</p>
<h3>Mettez en couleur</h3>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/inkscape-remplissage-et-contour-couleur-plus-sombre.png" alt="Inkscape remplissage plus sombre" width="345" height="442" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Couleur plus sombre grâce à l'outil TSL du "Remplissage et Contour" d'Inkscape</figcaption>
</figure>
<p>La meilleure façon de choisir les couleurs pour réaliser les lumières et les ombres est d'expérimenter avec la valeur Luminosité de l'onglet TSL de la palette de couleur "Remplissage et contour" d'Inkscape. Mais ce n'est pas le seul moyen.</p>
<h2>Réalisons, pour terminer, les finitions:</h2>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/4.png" alt="Inkscape finitions avatar fille" width="620" height="496" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Des finitions parfaites</figcaption>
</figure>
<h3>Dessinez</h3>
<p>Pour ajouter plus de diversité à votre avatar, vous pouvez lui mettre un bijou. Tout d'abord créez une perle avec l'outil Ellipse (F5 ou E), puis dessinez un chemin de collier et appliquez l'extension "Motif le long d'un chemin" (Extensions&gt;Générer à partir du chemin&gt;Motif le long d'un chemin...).
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/inkscape-menu-extension-motif-long-chemin.png" alt="Inkscape extension motif le long d'un chemin" width="620" height="379" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Extensions&gt;Générer à partir du chemin&gt;Motif le long d'un chemin...</figcaption>
</figure>
<blockquote><strong>Remarque:</strong>Dans Inkscape il est très important de placer la perle au premier plan (Objet&gt;Monter au premier plan), sinon l'extension ne fonctionnera pas comme souhaité. Pour voir le résultat à l'avance, veuillez cocher l'Aperçu en direct.
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/inkscape-motif-long-chemin.png" alt="Inkscape motif le long d'un chemin" width="392" height="397" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />utilisez l'aperçu en direct avant d'appliquer</figcaption>
</figure>
</blockquote></p>
<p>En conséquence, nous devrions obtenir un objet constitué de plusieurs perles le long du chemin. Ensuite, dupliquez l'objet résultant, remplissez-le avec une couleur plus claire et réduisez-le en faisant Chemin>Offset dynamique, puis repassez le en Chemin>Objet en chemin et placez-le correctement.</p>
<h3>Ajouter du volume</h3>
<p>
<ol>
<li>Ajoutez une ombre des cils avec l'outil Courbes de Bézier (Maj+F6). Remplissez le chemin avec une couleur bleu foncé et rendez-la aussi transparente que possible.</li>
<li>Ajoutez également plus de détails sur les cheveux - créez plusieurs lignes avec l'outil Courbes de Bézier (Maj+F6) en mode de forme "Ellipse".</li>
<li>L'étape finale consiste à ajouter des dégradés linéaires à: 
<ul><li>ombres sous les yeux;</li><li>reflets brillants roses sur les lèvres;</li><li>et un petit reflet sur le nez (créé avec l'outil Courbes de Bézier).</li>
</ol>
</p>
<h2>Conclusion : Utilisez ce modèle à l'envie</h2>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/5.png" alt="Inkscape avatar féminin fini" width="620" height="620" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Notre superbe avatar BD féminin</figcaption>
</figure>
<p>C'est tout ! Notre avatar BD féminin est prête. Vous êtes en mesure de créer plusieurs expressions faciales différentes basées sur son modèle. Par exemple: triste, langoureuse, joyeuse, en colère, apeurée, etc.</p>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-fille/emotions.png" alt="Inkscape avatar féminin émotions" width="621" height="275" >
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Essayez de donner des expressions émotives à votre avatar BD féminin</figcaption>
</figure>

<p>Voila, c'est fini, snif. Bon, mais même si vous ne comprenez pas l'anglais, vous devez absolument <a href="http://is.gd/hhCV4A">visiter le site de l'auteure: Olga Bukmullina</a>. Car c'est une professionnelle du graphisme 2D, pas comme moi, et surtout elle donne pleins de conseils en images... donc même pas besoin de savoir lire :D (bon, je suis un peu Marseillais). </p>
<p>Et en bonus, voici ce que j'ai réalisé a mon deuxième essai :<br />
<img class="ombre-image" src="data/medias/tuto-avatar-fille/avatar-fille.png" alt="avatar inkscape filles" /><br />
Bon, c'est sûr, c'est à améliorer, mais vous ne verrez pas mon premier essai <img src="plugins/plxtoolbar/custom.buttons/smilies/wink.png" alt=";-)" /></p>]]></content>
	<tags><![CDATA[inkscape, vectoriel, tutoriel]]></tags>
	<meta_description><![CDATA[Dessinez votre propre avatar facilement avec Inkscape]]></meta_description>
	<meta_keywords><![CDATA[Inkscape, tutoriel, dessiner avatar, volume et formes,]]></meta_keywords>
	<title_htmltag><![CDATA[Tutoriel Inkscape, dessiner un avatar BD féminin]]></title_htmltag>
	<vignette><![CDATA[tuto-avatar-fille/avatar-fille-0-titre-fr.tb.png]]></vignette>
</document>