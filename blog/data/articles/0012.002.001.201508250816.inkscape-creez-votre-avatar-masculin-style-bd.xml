<?xml version='1.0' encoding='UTF-8'?>
<document>
	<title><![CDATA[Inkscape : ﻿Créez votre avatar masculin style BD]]></title>
	<allow_com>1</allow_com>
	<template><![CDATA[article.php]]></template>
	<chapo><![CDATA[<p>Voici la suite de la traduction des tutoriels <strong>Inkscape</strong> de Olga Bikmullina sur les <strong>avatars façon BD</strong>.</p>

<p>L'idée de ces traductions est venue du fait que j'ai montré à ma petite soeur les tutos d'Olga, mais qu'elle ne comprenait pas les explications techniques en anglais.</p>

<p>Donc pour l'aider, j'ai contacté l'auteure. Elle a été ravie que son travail soit traduit en français et elle m'a donné son aval.</p>

<p>Je la remercie donc, d'abord pour la qualité de ses graphismes, pour le soin apporté dans ses tutoriels, et pour son aimable autorisation de reproduire l'article d'origine.</p>

<p>En suivant ce lien, vous trouverez <a href="http://ahninniah.blogspot.ru/2013/08/creating-male-cartoon-avatar-in-inkscape.html">l'original en anglais de ce tutoriel pour Inkscape</a></p>

<p>Maintenant, au boulot !</p>]]></chapo>
	<content><![CDATA[<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-mec/title.png" alt="Inkscape : ﻿Créez votre avatar masculin style BD" width="619" height="403" />
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />L'image originale du
    <br /><a href="http://ahninniah.blogspot.ru/2013/08/creating-male-cartoon-avatar-in-inkscape.html">tutoriel de Olga Bikmullina</a></figcaption>
</figure>

<p>Avant de commencer, si ce n'est déjà fait, vous devriez suivre le tutoriel précédent de cette série : <a href="http://dtar.fr/blog/article7/inkscape-dessinez-un-avatar-feminin">« Inkscape&nbsp;: Dessinez un avatar féminin style BD »</a>. De nombreuses manipulations de ce cours sont identiques à celles du précédent, donc elles ne seront pas expliquées.</p>

<p>Il s'agit de :</p>

<ul>
<li>paramétrer l'espace de travail,</li>
<li>créer les chemins,</li>
<li>choisir les couleurs ,</li>
<li>ajouter les volumes,</li>
<li>etc.</li>
</ul>

<p>Toutes ces manipulations sont détaillées dans <a href="http://dtar.fr/blog/article7/inkscape-dessinez-un-avatar-feminin">l'épisode précédent</a>.</p>


<h2>Dessinez les formes de base :</h2>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-mec/1.png" alt="Inkscape : ﻿formes de base de votre avatar masculin style BD" width="620" height="496" />
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Les formes de bases avec Inkscape</figcaption>
</figure>
<p>Créez les éléments de base du sujet :</p>

<ol>
<li>la tête,</li>
<li>les oreilles,</li>
<li>le cou,</li>
<li>l'ombre sous la tête,</li>
<li>et les épaules.</li>
</ol>

<p>Vous pouvez considérer que pour un avatar masculin, la tête semble plus carrée et les épaules sont plus anguleuses.</p>


<h2>Les yeux, les oreilles, la barbe et les cheveux&nbsp;:</h2>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-mec/2.png" alt="Inkscape : détails de votre avatar masculin style BD" width="620" height="496" />
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Les détails de votre avatar Inkscape</figcaption>
</figure>
<p>1. Faites un oeil plus arrondi. Les sourcils peuvent être créés de deux façons:</p>

<ul>
<li>en courbant la forme de base Ellipse (F5 ou E),</li>
<li>avec la courbe de Bézier (Maj+F6) en « style des nouveaux chemins desinés avec cet outil » « Ellipse ». Si la largeur du contour ne vous convient pas, vous pouvez convertir l'objet en chemin (Chemin>Objet en chemin ou Maj+Ctrl+C) et allez dans le menu Chemin>Offset dynamique (Ctrl+J). En faisant glisser le noeud blanc de la gauche vers la droite, vous modifiez la distance de décalage. Pour être en mesure d'éditer et  de déplacer le chemin après l'application de l'Offset dynamique, vous devez à nouveau convertir l'objet en chemin (Chemin>Objet en chemin ou Maj+Ctrl+C).</li>
</ul>

<p>2. Détaillez les oreilles, créez le chemin du nez, passez aux lèvres. Après la création de trois objets pour les lèvres faites le chemin du milieu plus sombre pour séparer visuellement les lèvres .</p>

<p>3. Ensuite, passez à la création d'une barbe avec l'outil Courbe de Bézier (Maj + F6) et la forme Rectangle (F4). Il est très probable que la barbe dépasse du chemin de la tête. Si ce n'est pas ce que vous aviez prévu au départ, unissez les deux objets créés (Path> Union) et faites intersection avec duplicata de la tête (Path> Intersection). Notez bien que l'utilisation des intersections (Ctrl+*) peut grandement simplifier votre travail.</p>

<p>4. Créez un chemin pour les cheveux, rendez les nœuds doux. Vous noterez ici que les cheveux sont placés au premier plan (Objet> Passer au premier-plan).</p>


<h2>Vêtement et petits détails&nbsp;:</h2>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-mec/3.png" alt="Inkscape : les vêtements de votre avatar masculin style BD" width="620" height="496" />
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Les vêtements de votre avatar Inkscape</figcaption>
</figure>
<p>1. Les vêtements, qui habillent notre sujet, peuvent symboliser son activité. Dans ce cas, nous avons choisi la variante la plus répandue : chemise et cravate. Ce style peut exprimer que notre avatar est un employé de bureau, ou une personne propre sur lui.</p>

<p>Créez un chemin pour la chemise et passez au col de chemise. Le col se compose de trois parties, l'une qui sera situé derrière le personnage (Objet>Descendre à l'arrière-plan). Les deux autres parties sont symétriques, pivotés horizontalement (H) et remplis d'une couleur plus claire.</p>

<p>La cravate peut être réalisée avec la forme de base Carré (Ctrl+F4 ou R).</p>

<ul>
<li>Créez un carré et pivotez-le de 45 degrés (en maintenant la touche Ctrl enfoncée)</li>
<li>Dupliquez (Ctrl+D) ce losange, étirez-le et ajoutez un nœud supplémentaire.</li>
<li>Dupliquez le rectangle de la chemise et faites « intersection » avec la cravate (Path>Intersection ou Ctrl+*).</li>
</ul>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-mec/tie.png" alt="Inkscape : ﻿la cravatte de votre avatar masculin style BD" width="620" height="496" />
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />La cravatte en partant de la forme de base Inkscape&nbsp;: 
<br />"&nbsp;Créer des rectangles et des carrés (F4)</figcaption>
</figure>

<p>2- ajoutez les détails aux cheveux, aux yeux et aux lèvres.</p>

<h2>Les finitions dans Inkscape&nbsp;:</h2>
<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-mec/4.png" alt="Inkscape : ﻿les finitions de votre avatar masculin style BD" width="620" height="496" />
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Les finitions de l'avatar Inkscape</figcaption>
</figure>
<p>Les finitions comprennent la création d'ombres sous le col de la chemise, l'ajout d'une surbrillance sur le nez, des détails pour les cheveux et les yeux, l'ajout d'un volume à la cravate avec des éléments clairs.</p>

<blockquote>
<p><em>Remarque:</em><br />
Certains éléments qui rendent votre avatar trop détaillé (reflet de l'iris, les lèvres et le nez) peuvent être omis. Surtout si vous prévoyez d'utiliser l'avatar seulement en basse résolution.</p></blockquote>

<h2>Rendez votre avatar Inkscape plus vivant&nbsp;:</h2>

<p>C'est tout ! Votre avatar BD masculin est prêt. Pour le rendre plus vivant, vous pouvez expérimenter avec ses expressions faciales.</p>

<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-mec/final.png" alt="Inkscape : ﻿Votre avatar masculin style BD" width="620" height="620" />
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Votre Avatar Inkscape masculin style BD fini&nbsp;!</figcaption>
</figure>

<p>Une émotion comme la surprise peut être exprimée avec la bouche arrondie, de grands yeux et les sourcils levés. La joie s'exprimée à travers un large sourire et le bonheur avec un grand sourire et les sourcils levés. Une expression sournoise ou blasée peut être réalisée avec les yeux plissés, un sourcil levé, et avec un rictus à la place du sourire...</p>

<p>Lorsque vous créez une expression du visage vous devez tenir compte de la position et de la forme de toute les parties du visage comme les sourcils, les yeux et la bouche. Vous pouvez même expérimenter en face du miroir et essayer de dépeindre différentes émotions avec vous comme référence.</p>

<figure>
<img class="ombre-image" src="data/medias/tuto-avatar-mec/expressions.png" alt="Inkscape : ﻿les expressions de votre avatar masculin style BD" width="620" height="254" />
<figcaption style="clear: left;margin: .75em 0;text-align: center;font-style: italic;line-height: 1.5em;"
		<br />Expérimentez quelques expressions en vous regardant dans une glace :D </figcaption>
</figure>

<p>Si à la lecture de ce tutoriel Inkscape vous avez trouvé des erreurs dans le texte ou si vous avez une meilleure solution sentez-vous libre de commenter. Merci&nbsp;!</p>

<p style="text-align:right"><b><a href="http://openclipart.org/detail/181856/male-cartoon-avatar--by-ahninniah-181856">>>Téléchargez le fichier source « Male Cartoon Avatar » (.svg)</a></b></p>]]></content>
	<tags><![CDATA[inkscape, tutoriel, vectoriel]]></tags>
	<meta_description><![CDATA[Dessinez votre propre avatar facilement avec Inkscape]]></meta_description>
	<meta_keywords><![CDATA[Inkscape, tutoriel, avatar,]]></meta_keywords>
	<title_htmltag><![CDATA[Tutoriel Inkscape, dessiner un avatar BD masculin]]></title_htmltag>
	<vignette><![CDATA[tuto-avatar-mec/final.tb.png]]></vignette>
</document>