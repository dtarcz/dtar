<?php
global $plxShow;
eval($plxShow->callHook("MyCoinSlider"));
?>


<h2>Bonjour et bienvenue sur mon blog.</h2>

<p>Il est difficile de parler de soi lorsque ce n'est pas habituel, alors je reviendrai sur cette page améliorer ma présentation quand je serai familiarisé avec l'écriture.</p>

<p>Mon nom est David Tarczewski, j'ai 45 ans cette année et je vis dans le sud de la France.</p>

<p>Pratiquant d'Aïkido et de Reïki (1er degré), j'aime marcher mais j'ai une vie sédentaire en ce moment...</p>

<p>Je m'intéresse au développement personnel et je suis un fervent militant pour une agriculture plus saine et respectueuse de l'environnement.</p>

<p>Je joue de la guitare mais pas assez souvent pour me dire guitariste :) .</p>

<h2>Pourquoi faire ce blog ?</h2>

<h3>Le Graphisme</h3>

<p>D'abord avec The Gimp, et maintenant avec Inkscape, j'utilise les outils numériques pour pratiquer le dessin.</p>

<h3>Le Reïki</h3>

<p>Initié niveau 1 depuis septembre 2006, je ne suis pas toujours ouvert au Reïki... Sporadiquement je m'en éloigne, pour le retrouver chaque fois que j'en ai besoin ! Et oui, c'est ça aussi la grande force du Reïki : Une fois initié, il est là, présent pour nous (bien évidement car c'est "l'Énergie Universelle"). Mais bien souvent c'est nous qui ne sommes pas disponibles ;)</p>

<p>Alors j'ai décidé le jour de mes 42 ans de faire quelque chose de grandiose pour moi : Rester dans le Reïki !</p>

<p>Et pour ce faire, je n'ai rien trouvé de mieux que de tenir ce journal, qui sera public, Pour partager avec vous ce que je vis dans le Reïki...</p>

<p>Vous allez bénéficier de plus de dix ans de recherches personnelles pour s'améliorer, pour se connaître, pour aller mieux, et pour aider les autres !</p>

<h3>La Décroissance</h3>

<p>Des idées pour vivre en harmonie et dans le <strong>respect de la planète, des animaux, des autres et de soi-même</strong>.</p>

<p><img alt="Image flottante de vélo" class="imageflottante" src="data/medias/velo.png" /> Voici un vélo, qui représente pour moi à l'heure actuelle la meilleure façon de se déplacer. On profite du bon air, et on ne polue pas. Argument non négligeable, c'est très économique comme mode de déplacement !</p>

<h3>Le Revenu de Base</h3>

<p>Connaissez-vous cette idée qui fait son chemin dans les consciences planétaires ? Celle qui dit que chaque être humain a droit a son petit lopin de terre. Pour pouvoir en vivre, y vivre...</p>

<p>Mais comme évidemment il n'y a pas vraiment de parcelle pour tout le monde, on remplace cet espace d'autosuffisance par un revenu.</p>

<p>Ce revenu de base devra être alloué à tous, sans aucune distinction d'âge, de sexe, etc. Et à vie. De la naissance à la mort.</p>

<p>Et bien ici, vous pourrez lire des articles sur cette idée. En tout cas, vous trouverez ici des arguments qui se tiennent en faveur de cette petite révolution sociétale...</p>

<h3>Le Jeu</h3>

<p>S'amuser, c'est aussi une excellente façon de faire connaissance :)</p>

<h4>Le jeu de rôle</h4>

<h4>le jeu de GO</h4>

<h4>Le jeu en ligne</h4>

<p>Exclusivement des jeux Libres et Gratuits, multi-joueurs, pour passer de bons moments dans la convivialité.</p>

<h5>Globulation2</h5>

<p><img alt="Image flottante" class="imageflottante" src="data/medias/glob2-icon-48x48.png" /> Aaah, voilà un jeu absolument génial ! Globulation 2 est un jeu de Stratégie Temps Réel (RTS Real-Time Strategy) innovant qui réduit la gestion du joueur en assignant automatiquement des tâches aux unités. Et bien que ce soit aussi un jeu de conquête, il est complètement adapté aux plus jeunes, je trouve.</p>

<h5>Landes Éternelles</h5>

<p><img alt="Image flottante" class="imageflottante" src="data/medias/icon-le.bmp" /> Un jeu véritablement fascinant en 3D. On y incarne un personnage qui évolue au gré de nos envies... Carrément du jeu de rôle avec support graphique.</p>

<h5>The Mana World</h5>

<p><img alt="Image flottante" class="imageflottante" src="data/medias/Tmw_logo-mini.png" /> Un petit jeu très mignon en 2D, sur lequel on peut passer des heures, là aussi pour faire évoluer un personnage.</p>

<h5>Open TTD</h5>

<p><img alt="Image flottante" class="imageflottante" src="data/medias/openttd.32.bmp" /> On change de registre, pour construire un réseau de transport. Chemin de fer, bateaux, et transport routier, serez-vous capable de faire prospérer votre petite société ? Vous pourrez même faire grandir des villes, créer des industries, et construire des gares et des réseaux de transports hallucinants.</p>

<h5>FreeCiv</h5>

<p><img alt="Image flottante" class="imageflottante" src="data/medias/civicon.png" /> Vous démarrez avec un petit colon et un explorateur, pour créer une civilisation capable d'aller dans les étoiles... Avec FreeCiv, vous faites évoluer une civilisation de 4000 ans avant J.C. jusque dans le futur.</p>

<h5>FreeCol</h5>

<p><img alt="Image flottante" class="imageflottante" src="data/medias/freecol-mini.png" /> Petit frère de FreeCiv, sur le continent des Amériques. Vous incarnez une civilisation européenne en 1360 qui découvre et colonise le nouveau monde, pour finalement s'émanciper de sa métropole.</p>