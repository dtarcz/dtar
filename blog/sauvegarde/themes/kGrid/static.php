<?php include(dirname(__FILE__).'/header.php'); ?>

<div class="row">
  <div class="c9">
    <div class="wrap">

			<article role="article" class="static">

				<section>
					<?php $plxShow->staticContent(); ?>
				</section>

			</article>

		</div>

    </div>

      
      <?php include(dirname(__FILE__).'/sidebar.php'); ?>
      
</div>
      
<?php include(dirname(__FILE__).'/footer.php'); ?>
