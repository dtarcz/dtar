<?php include(dirname(__FILE__).'/header.php'); ?>

<div class="row">
  <div class="c12">
    <div class="wrap">

			  <article class="art-content" id="post-<?php echo $plxShow->artId(); ?>">
          
          <header role="banner">

              <h2>
						    <?php $plxShow->artTitle(''); ?>
					    </h2>
              
              <div role="contentinfo" class="meta-info">
					     <p>
						    <?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?> /
						    <?php $plxShow->artDate('#num_day #month #num_year(4)'); ?> /
						    <?php $plxShow->artNbCom(); ?>
                <br />
                <?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat(); ?> /
						    <?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags(); ?>
					     </p>
              </div>
          
          </header>  
          
          <section>
					   <?php $plxShow->artcontent(); ?>
				  </section>
          
        </article>

			<?php $plxShow->artAuthorInfos('<div class="author-infos">#art_authorinfos</div>'); ?>

      <?php include(dirname(__FILE__).'/commentaires.php'); ?>

		</div>
  </div>
</div>

<?php include(dirname(__FILE__).'/footer.php'); ?>

