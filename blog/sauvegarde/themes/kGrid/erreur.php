<?php include(dirname(__FILE__).'/header.php'); ?>

<div class="row">
  <div class="c9">
    <div class="wrap">

			<article role="article">

				<header role="banner">
					<h1>
						<?php $plxShow->lang('ERROR'); ?>
					</h1>
				</header>

				<section>
					<p>
						<?php $plxShow->erreurMessage(); ?>
					</p>
				</section>

			</article>

    </div>
  </div>
      
      <?php include(dirname(__FILE__).'/sidebar.php'); ?>
      
</div>
      
<?php include(dirname(__FILE__).'/footer.php'); ?>
