<?php if (!defined('PLX_ROOT')) exit; ?>
 
<footer class="copyright">
    <div class="row" id="footer">
		  <div class="c12">
        <div class="wrap-footer blog-footer">      
	      
        <p>
			     <?php $plxShow->mainTitle('link'); ?> @ 2013 - <?php $plxShow->subTitle(); ?>
		    </p>
        
        <p>
			     <?php $plxShow->lang('POWERED_BY') ?> <a href="http://www.pluxml.org" title="<?php $plxShow->lang('PLUXML_DESCRIPTION') ?>">PluXml</a>
			     <?php $plxShow->lang('IN') ?> <?php $plxShow->chrono(); ?> - Thème par <a href="http://www.kamea.net">kamea.net</a> 
			     
          <span class="flr">
           <a rel="nofollow" href="<?php $plxShow->urlRewrite('core/admin/'); ?>" title="<?php $plxShow->lang('ADMINISTRATION') ?>"><?php $plxShow->lang('ADMINISTRATION') ?></a> 
			      - <a href="#thetop">En haut</a> 
          </span>
			   <?php $plxShow->httpEncoding() ?>
		    </p>
          
        </div>
			</div>
    </div>
</footer>

</div>

<script type="text/javascript">

		$(function() {
			var pull 		= $('#pull');
				menu 		= $('nav ul');
				menuHeight	= menu.height();

			$(pull).on('click', function(e) {
				e.preventDefault();
				menu.slideToggle();
			});

			$(window).resize(function(){
        		var w = $(window).width();
        		if(w > 320 && menu.is(':hidden')) {
        			menu.removeAttr('style');
        		}
    		});
		});

</script>
  
  </body>
</html>

