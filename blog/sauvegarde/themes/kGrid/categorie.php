<?php include(dirname(__FILE__).'/header.php'); ?>

<div class="row">
  <div class="c9">
    <div class="wrap">
    
      <p class="meta-info cat-info p10">
				<?php $plxShow->lang('SEARCH_CLASSIFIED'); ?> <span><?php $plxShow->catName(); ?></span>
			</p>

			<?php while($plxShow->plxMotor->plxRecord_arts->loop()): ?>

			<article class="art-content" id="post-<?php echo $plxShow->artId(); ?>">
          
          <header role="banner">

              <h2>
						    <?php $plxShow->artTitle('link'); ?>
					    </h2>
              
              <div role="contentinfo" class="meta-info">
					     <p>
						    <i class="icon-user color1"></i>&nbsp;<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>&nbsp;
						    <i class="icon-calendar color1"></i>&nbsp;<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>&nbsp;
						    <i class="icon-comments-alt color1"></i>&nbsp;<?php $plxShow->artNbCom(); ?>
                <br />
                <i class="icon-eye-open color1"></i>&nbsp;<?php $plxShow->lang('CLASSIFIED_IN') ?><?php $plxShow->artCat(); ?>&nbsp;
						    <i class="icon-tags color1"></i>&nbsp;<?php $plxShow->lang('TAGS') ?><?php $plxShow->artTags(); ?>
					     </p>
              </div>
          
          </header>  
          
          <section>
					   <?php $plxShow->artChapo(); ?>
				  </section>
          
      </article>

			<?php endwhile; ?>

			<div id="pagination">
				<?php $plxShow->pagination(); ?>
			</div>

			<div class="rss">
				<?php $plxShow->artFeed('rss',$plxShow->catId()); ?>
			</div>

    </div>
  </div>
      
      <?php include(dirname(__FILE__).'/sidebar.php'); ?>
      
</div>
      
<?php include(dirname(__FILE__).'/footer.php'); ?>
