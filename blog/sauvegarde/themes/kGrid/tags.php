<?php include(dirname(__FILE__).'/header.php'); ?>

<div class="row">
  <div class="c9">
    <div class="wrap">

			<p class="meta-info tag-info p10">
				<?php $plxShow->lang('SEARCH_TAGS'); ?> <span><?php echo $plxShow->plxMotor->cible; ?></span>
			</p>

			<?php while($plxShow->plxMotor->plxRecord_arts->loop()): ?>

			<article class="art-content" id="post-<?php echo $plxShow->artId(); ?>">
          
          <header role="banner">

              <h2>
						    <?php $plxShow->artTitle('link'); ?>
					    </h2>
              
              <footer role="contentinfo" class="meta-info">
					     <p>
						    <?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?> /
						    <?php $plxShow->artDate('#num_day #month #num_year(4)'); ?> /
						    <?php $plxShow->artNbCom(); ?>
                <br />
                <?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat(); ?> /
						    <?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags(); ?>
					     </p>
              </footer>
          
          </header>  
          
          <section>
					   <?php $plxShow->artChapo(); ?>
				  </section>
          
      </article>

			<?php endwhile; ?>

			<div id="pagination">
				<?php $plxShow->pagination(); ?>
			</div>

			<div class="rss">
				<?php $plxShow->tagFeed() ?>
			</div>

    </div>
  </div>
      
      <?php include(dirname(__FILE__).'/sidebar.php'); ?>
      
</div>
      
<?php include(dirname(__FILE__).'/footer.php'); ?>
