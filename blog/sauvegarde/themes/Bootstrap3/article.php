<?php include(dirname(__FILE__).'/header.php'); ?>
    <div class="container">

      <!-- FIL D'ARIANE -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="./"><?php $plxShow->lang('HOME'); ?></a></li>
            <li><?php $plxShow->lang('BLOG'); ?></li>
            <li class="active"><?php $plxShow->artTitle(''); ?></li>
          </ol>
        </div>
      </div>
      <!-- /FIL D'ARIANE -->

      <div class="row">

        <div class="col-lg-8">
        
          <div id="post-<?php echo $plxShow->artId(); ?>">
            <h1><?php $plxShow->artTitle(''); ?></h1>
            <p class="lead"><i class="icon-user"></i> <?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor(); ?> <i class="icon-comments-alt"></i> <?php $plxShow->artNbCom('L_NO_COMMENT', '#nb', '#nb'); ?></p>
          <hr>
            <p><i class="icon-time"></i> <?php $plxShow->artDate('#num_day #month #num_year(4)'); ?> <i class="icon-folder-close"></i> <?php $plxShow->artCat(); ?> <i class="icon-tags"></i>  <?php $plxShow->artTags('<a class="label label-danger #tag_status" href="#tag_url" title="#tag_name">#tag_name</a>', ''); ?></p>
          <hr>
          <?php $plxShow->artContent(); ?>
          <hr>
          </div><!-- /post-<?php echo $plxShow->artId(); ?> -->

			<?php $plxShow->artAuthorInfos('<blockquote>#art_authorinfos</blockquote>'); ?>

			<?php include(dirname(__FILE__).'/commentaires.php'); ?>

        </div><!-- /col-lg-8 -->

		<?php include(dirname(__FILE__).'/sidebar.php'); ?>

      </div><!-- /row -->

    </div><!-- /.container -->
<?php include(dirname(__FILE__).'/footer.php'); ?>
