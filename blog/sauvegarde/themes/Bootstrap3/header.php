<?php if (!defined('PLX_ROOT')) exit; ?>
<!DOCTYPE html>
<html lang="<?php $plxShow->defaultLang() ?>">
  <head>
    <meta charset="<?php $plxShow->charset('min'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php $plxShow->pageTitle(); ?></title>
    <?php $plxShow->meta('description') ?>
    <?php $plxShow->meta('keywords') ?>
    <?php $plxShow->meta('author') ?>
    <link rel="icon" href="<?php $plxShow->template(); ?>/favicon-d.png" />
    
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php $plxShow->template(); ?>/css/minified.css.php" media="screen"/>

    <!-- Add custom CSS here -->
    <link href="<?php $plxShow->template(); ?>/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <?php $plxShow->templateCss() ?>
    <?php #$plxShow->pluginsCss() ?>
    <link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" />
    <link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" />
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top purple" role="navigation">
      <div class="container">
      
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $plxShow->urlRewrite() ?>"><i class="icon-leaf"></i> <?php $plxShow->mainTitle(); ?></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">       
          <ul class="nav navbar-nav navbar-right">
			<?php $plxShow->staticList($plxShow->getLang('HOME'),'<li class="#static_status"><a href="#static_url" title="#static_name">#static_name</a></li>'); ?>
			<?php $plxShow->pageBlog('<li class="#page_status" id="#page_id"><a href="#page_url" title="#page_name">#page_name</a></li>'); ?>
			
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-rss"></i>  <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS'); ?>"><?php $plxShow->lang('ARTICLES'); ?></a></li>
                <li><a href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires'); ?>" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>"><?php $plxShow->lang('COMMENTS'); ?></a></li>
              </ul>
            </li>
            
          </ul>
        </div><!-- /.navbar-collapse -->
        
      </div><!-- /.container -->
    </nav>
