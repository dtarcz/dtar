<?php

$LANG = array(

#header.php
'HOME'					=> 'Startseite',
'BLOG'					=> 'Blog',
'GOTO_CONTENT'			=> 'Zum Inhalt',
'GOTO_MENU'				=> 'Zum Men&uuml;',
'COMMENTS_RSS_FEEDS'	=> 'Kommentare als Feed',
'COMMENTS'				=> 'Kommentare',
'ARTICLES_RSS_FEEDS'	=> 'Artikel-Feed (RSS)',
'ARTICLES'				=> 'Artikel',

# sidebar.php
'CATEGORIES' 			=> 'Kategorien',
'LATEST_ARTICLES'		=> 'Letzte Artikel',
'LATEST_COMMENTS'		=> 'Letzte Kommentare',
'ARCHIVES'				=> 'Archive',

# footer.php
'POWERED_BY'			=> 'Powered by',
'DESIGN_BY'             => '<a href="http://forum.pluxml.org/profile.php?id=2985"><i class="icon-magic"></i> Réalisé par Fred</a>',
'PLUXML_DESCRIPTION'	=> 'Blog oder Cms ohne Datenbank',
'IN'					=> 'in',
'ADMINISTRATION'		=> 'Admin',
'GOTO_TOP'				=> 'Zum Seitenanfang',
'TOP'					=> 'Top',

# erreur.php
'ERROR'					=> 'Ein Fehler ist erkannt geworden',
'BACKTO_HOME'			=> 'Zur&uuml;ck zur Homepage',

# common
'WRITTEN_BY'			=> 'Geschrieben von',
'CLASSIFIED_IN'			=> 'Kategorisiert in',
'TAGS'					=> 'Schl&uuml;sselw&ouml;rter',

# commentaires.php
'SAID'					=> 'sagte',
'WRITE_A_COMMENT'		=> 'Schreiben einen Kommentar',
'NAME'					=> 'Name',
'WEBSITE'				=> 'Website (fakultativ)',
'EMAIL'					=> 'E-mail (fakultativ)',
'COMMENT'				=> 'Kommentar',
'CLEAR'					=> 'L&ouml;schen',
'SEND'					=> 'Versenden',
'COMMENTS_CLOSED'		=> 'Die Kommentare sind geschlossen',
'ANTISPAM_WARNING'		=> 'Anti-Spam',
);

?>