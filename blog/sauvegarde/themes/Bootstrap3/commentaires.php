<?php if(!defined('PLX_ROOT')) exit; ?>

	<?php if($plxShow->plxMotor->plxRecord_coms): ?>
          
	<div id="comments">
		<h2><?php echo $plxShow->artNbCom(); ?></h2>
          <hr>
          <!-- the comments -->
          <?php while($plxShow->plxMotor->plxRecord_coms->loop()): # On boucle sur les commentaires ?>
          <h3 id="<?php $plxShow->comId(); ?>"><?php $plxShow->comAuthor('link'); ?> <small><a class="num-com" href="<?php $plxShow->ComUrl(); ?>" title="#<?php echo $plxShow->plxMotor->plxRecord_coms->i+1 ?>">#<?php echo $plxShow->plxMotor->plxRecord_coms->i+1 ?></a> <?php $plxShow->comDate('#day #num_day #month #num_year(4) &#64; #hour:#minute'); ?></small></h3>
          <p class="content_com type-<?php $plxShow->comType(); ?>"><?php $plxShow->comContent(); ?></p>
          <hr>          
          <?php endwhile; # Fin de la boucle sur les commentaires ?>
          
		  <div class="label label-warning pull-right">
			<i class="icon-rss-sign"></i> <?php $plxShow->comFeed('rss',$plxShow->artId()); ?>
		  </div>             
	</div>

	<?php endif; ?>

	<?php if($plxShow->plxMotor->plxRecord_arts->f('allow_com') AND $plxShow->plxMotor->aConf['allow_com']): ?>

	<div id="form">

          <div class="well">
            <h4><?php $plxShow->lang('WRITE_A_COMMENT') ?></h4>
            <form action="<?php $plxShow->artUrl(); ?>#form" method="post">
              <?php $plxShow->comMessage('<div class="alert alert-danger">#com_message</div>'); ?>
              
              <div class="form-group col-lg-4">
                <label for="id_name"><?php $plxShow->lang('NAME') ?></label>
                <input id="id_name" name="name" type="text" value="<?php $plxShow->comGet('name',''); ?>" maxlength="30" class="form-control">
              </div>
              
              <div class="form-group col-lg-4">
                <label for="id_mail"><?php $plxShow->lang('EMAIL') ?></label>
                <input id="id_mail" name="mail" type="email" value="<?php $plxShow->comGet('mail',''); ?>" class="form-control">
              </div>
              
              <div class="form-group col-lg-4">
                <label for="id_site"><?php $plxShow->lang('WEBSITE') ?></label>
                <input id="id_site" name="site" type="url" class="form-control" value="<?php $plxShow->comGet('site',''); ?>">
              </div>
              
              <div class="clearfix"></div>
              <div class="form-group col-lg-12">
                <label for="id_content"><?php $plxShow->lang('COMMENT') ?></label>
                <textarea  id="id_content" name="content" cols="35" rows="6"class="form-control"><?php $plxShow->comGet('content',''); ?></textarea>
              </div>

			  <?php if($plxShow->plxMotor->aConf['capcha']): ?>
              <div class="form-group col-lg-12">
                <label for="id_rep"><?php echo $plxShow->lang('ANTISPAM_WARNING') ?></label>
                <?php $plxShow->capchaQ(); ?> : <input id="id_rep" name="rep" type="text" size="2" maxlength="1" />
              </div>
			  <?php endif; ?>
			                
              <div class="form-group col-lg-12">
                <button type="submit" class="btn btn-primary"><?php $plxShow->lang('SEND') ?></button>
              </div>

            </form>
          </div>

	</div>

	<?php else: ?>

		<p>
			<?php $plxShow->lang('COMMENTS_CLOSED') ?>.
		</p>

	<?php endif; # Fin du if sur l'autorisation des commentaires ?>
