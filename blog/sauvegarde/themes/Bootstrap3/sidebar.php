<?php if(!defined('PLX_ROOT')) exit; ?>
        <!-- SIDEBAR -->
        <div class="col-lg-4">
          
          <div class="well">
            <h4><?php $plxShow->lang('CATEGORIES'); ?> <span class="pull-right"><?php $plxShow->lang('ARCHIVES'); ?></span></h4>
              <div class="row">
                <div class="col-lg-6">
                  <ul class="nav nav-pills nav-stacked">
                    <?php $plxShow->catList('','<li class="#cat_status" id="#cat_id"><a href="#cat_url" title="#cat_name"><span class="badge pull-right">#art_nb</span>#cat_name</a></li>'); ?>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="nav nav-pills nav-stacked">
                    <?php $plxShow->archList('<li class="#archives_status" id="#archives_id"><a href="#archives_url" title="#archives_name"><span class="badge pull-right">#archives_nbart</span>#archives_month #archives_year</a></li>'); ?>
                  </ul>
                </div>
              </div>
          </div><!-- /well -->
         
          
          <div class="well">
            <h4><?php $plxShow->lang('LATEST_ARTICLES'); ?></h4>
              <div class="row">
                <div class="col-lg-12">
                  <ul class="list-unstyled">
                    <?php $plxShow->lastArtList('<li><a class="#art_status" href="#art_url" title="#art_title">#art_title</a></li>'); ?>
                  </ul>
                </div>
              </div>
          </div><!-- /well -->
          
          
          <div class="well">
            <h4><?php $plxShow->lang('LATEST_COMMENTS'); ?></h4>
              <div class="row">  
                <div class="col-lg-12">
                  <ul class="list-unstyled">
                    <?php $plxShow->lastComList('<li><a href="#com_url">#com_author '.$plxShow->getLang('SAID').' : #com_content(34)</a></li>'); ?>
                  </ul>
                </div>
              </div>
          </div><!-- /well -->
          
                  
          <div class="well">
            <h4><?php $plxShow->lang('TAGS'); ?></h4>
              <div class="row">
                <div class="col-lg-12">
                  <ul class="list-inline">
                    <?php $plxShow->tagList('<li class="tag #tag_size"><a class="label label-danger #tag_status" href="#tag_url" title="#tag_name">#tag_name</a></li>', 20); ?>
                  </ul>
                </div>
              </div>
          </div><!-- /well -->  
          
          <div class="well">
            <h4>Statistiques</h4>
              <div class="row">
                <div class="col-lg-12">
                  <ul class="list-unstyled">
                    <li><?php $plxShow->nbAllArt('L_NO_ARTICLE','#nb L_ARTICLE','#nb L_ARTICLES'); ?></li>
                    <li><?php $plxShow->nbAllCom('L_NO_COMMENT','#nb L_COMMENT','#nb L_COMMENTS'); ?></li>
                    <li>PluXml <?php $plxShow->version() ?></li>
                  </ul>
                </div>
              </div>
          </div><!-- /well -->            
                  
        </div><!-- /col-lg-4 -->
        <!-- /SIDEBAR -->