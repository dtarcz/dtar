<?php include(dirname(__FILE__).'/header.php'); ?>
    <div class="container">

      <div class="row">

        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="./"><?php $plxShow->lang('HOME'); ?></a></li>
            <li class="active"><?php $plxShow->lang('ERROR'); ?></li>
          </ol>
        </div>

      </div>

      <div class="row">

        <div class="col-lg-12 text-center">
          <p class="error-404"><i class="icon-warning-sign"></i> 404</p>
          <p class="lead"><?php $plxShow->erreurMessage(); ?></p>
          <p><a class="btn btn-primary btn-lg" href="javascript:history.back()"><i class="icon-reply"></i> Retour</a></p>
        </div>

      </div>

    </div><!-- /.container -->
<?php include(dirname(__FILE__).'/footer.php'); ?>