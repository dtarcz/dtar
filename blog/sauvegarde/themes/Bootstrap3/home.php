<?php include(dirname(__FILE__).'/header.php'); ?>
    <div class="container">

      <!-- FIL D'ARIANE -->
      <div class="row">
        <div class="col-lg-12">
          <ol class="breadcrumb">
            <li><a href="./"><?php $plxShow->lang('HOME'); ?></a></li>
            <?php $plxShow->pageBlog('<li class="#page_status"><a href="#page_url" title="#page_name">#page_name</a></li>'); ?>
          </ol>
        </div>
      </div>
      <!-- /FIL D'ARIANE -->

      <div class="row">

        <div class="col-lg-8">
        
          <?php while($plxShow->plxMotor->plxRecord_arts->loop()): ?>
          <div id="post-<?php echo $plxShow->artId(); ?>">
            <h1><?php $plxShow->artTitle('link'); ?></h1>
            <p class="lead"><i class="icon-user"></i> <?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor(); ?> <i class="icon-comments-alt"></i> <?php $plxShow->artNbCom('L_NO_COMMENT', '#nb', '#nb'); ?></p>
          <hr>
            <p><i class="icon-time"></i> <?php $plxShow->artDate('#num_day #month #num_year(4)'); ?> <i class="icon-folder-close"></i> <?php $plxShow->artCat(); ?> <i class="icon-tags"></i>  <?php $plxShow->artTags('<a class="label label-danger #tag_status" href="#tag_url" title="#tag_name">#tag_name</a>', ''); ?></p>
          <hr>
          <?php $plxShow->artChapo(); ?>
          <hr>
          </div><!-- /post-<?php echo $plxShow->artId(); ?> -->
          <?php endwhile; ?>

		  <div class="label label-warning pull-right">
			<i class="icon-rss-sign"></i> <?php $plxShow->artFeed('rss',$plxShow->catId()); ?>
		  </div>          
          
          <div class="pagination">
            <?php $plxShow->pagination(); ?>
          </div>
			
			
        </div><!-- /col-lg-8 -->
        
		<?php include(dirname(__FILE__).'/sidebar.php'); ?>

      </div><!-- /row -->

    </div><!-- /.container -->
<?php include(dirname(__FILE__).'/footer.php'); ?>