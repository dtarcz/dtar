<?php if(!defined('PLX_ROOT')) exit; ?>

	<div id="footer">

		<p><?php $plxShow->mainTitle('link'); ?> -
			<?php $plxShow->lang('POWERED_BY') ?> <a href="http://pluxml.org" title="<?php $plxShow->lang('PLUXML_DESCRIPTION') ?>">PluXml</a>
			<?php $plxShow->lang('IN') ?> <?php $plxShow->chrono(); ?>
			<?php $plxShow->httpEncoding() ?>
      
      &nbsp;|&nbsp;
      Th&egrave;me par <a href="http://www.kamea.net" class="dott">kamea.net</a>
      &nbsp;|&nbsp;
      <a href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>"><?php $plxShow->lang('ARTICLES') ?></a>
      &nbsp;|&nbsp;
      <a href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>"><?php $plxShow->lang('COMMENTS') ?></a>
       
			<span><a class="admin" rel="nofollow" href="<?php $plxShow->urlRewrite('core/admin/') ?>" title="<?php $plxShow->lang('ADMINISTRATION') ?>"><?php $plxShow->lang('ADMINISTRATION') ?></a> -
			<a class="top" href="<?php echo $plxShow->urlRewrite('#top') ?>" title="<?php $plxShow->lang('GOTO_TOP') ?>"><?php $plxShow->lang('TOP') ?></a></span>
		</p>

	</div>
  

  
  <script type="text/javascript" src="themes/equinoxe/scripts/slider/js/jquery.min.js"></script>
  <script type="text/javascript" src="themes/equinoxe/scripts/slider/js/jquery.easing-sooper.js"></script>
  <script type="text/javascript" src="themes/equinoxe/scripts/slider/js/image_fade.js"></script>


</body>
</html>