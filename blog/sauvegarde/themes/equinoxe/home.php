<?php include(dirname(__FILE__).'/header.php'); ?>

	<div id="section">

		<div id="article">

			<?php while($plxShow->plxMotor->plxRecord_arts->loop()): ?>
				<h2><?php $plxShow->artTitle('link'); ?></h2>
				<p class="art-topinfos"><?php $plxShow->lang('WRITTEN_BY') ?> <?php $plxShow->artAuthor() ?> - <?php $plxShow->artDate('#num_day #month #num_year(4)'); ?></p>
				<p class="art-infos"><?php $plxShow->artNbCom(); ?> - <?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat(); ?> - <?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags(); ?></p>
				<div class="art-chapo"><?php $plxShow->artChapo(); ?></div>
			<?php endwhile; ?>

			<p id="pagination"><?php $plxShow->pagination(); ?></p>

		</div>

		<?php include(dirname(__FILE__).'/sidebar.php'); ?>

	</div>

<?php include(dirname(__FILE__).'/footer.php'); ?>
