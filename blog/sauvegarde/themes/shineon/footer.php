<?php if(!defined('PLX_ROOT')) exit; ?>

<div id="footer"> 
  
 <div>
  <?php $plxShow->mainTitle('link'); ?> -
	<?php $plxShow->lang('POWERED_BY') ?> <a href="http://www.pluxml.org" title="<?php $plxShow->lang('PLUXML_DESCRIPTION') ?>">PluXml</a>
	<?php $plxShow->lang('IN') ?> <?php $plxShow->chrono(); ?>
	<?php $plxShow->httpEncoding() ?> - Th&egrave;me par <a href="http://www.kamea.net" >Kamea</a>
	
  <div class="minilink_bas">
   <a href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>"><?php $plxShow->lang('ARTICLES') ?></a> - 
   <a class="admin" rel="nofollow" href="<?php $plxShow->urlRewrite('core/admin/') ?>" title="<?php $plxShow->lang('ADMINISTRATION') ?>"><?php $plxShow->lang('ADMINISTRATION') ?></a> -
	 <a class="top" href="<?php echo ('#top') ?>" title="<?php $plxShow->lang('GOTO_TOP') ?>"><?php $plxShow->lang('TOP') ?></a>
  </div>
    
  </div>

</div>
  
</div> <!-- Je ferme l'enveloppe !-->

<script type="text/javascript">  

    function juizScrollTo(element){  
        $(element).click(function(){  
            var goscroll = false;  
            var the_hash = $(this).attr("href");  
            var regex = new RegExp("\#(.*)","gi");  
            var the_element = '';  
       
            if(the_hash.match("\#(.+)")) {  
                the_hash = the_hash.replace(regex,"$1");  
       
                if($("#"+the_hash).length>0) {  
                    the_element = "#" + the_hash;  
                    goscroll = true;  
                }  
                else if($("a[name=" + the_hash + "]").length>0) {  
                    the_element = "a[name=" + the_hash + "]";  
                    goscroll = true;  
                }  
       
                if(goscroll) {  
                    $('html, body').animate({  
                        scrollTop:$(the_element).offset().top  
                    }, 'slow');  
                    return false;  
                }  
            }  
        });  
    };  
    juizScrollTo('a[href^="#"]');  
   
</script>

</body>
</html>
