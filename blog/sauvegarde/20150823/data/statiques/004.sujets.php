<p>Bienvenue sur ce <em>modeste site</em> pour <strong>l'Amour, la Paix, la Joie...</strong>.</p>

<p>Merci <strong>à toi</strong> de le visiter.</p>

<p>Tu trouveras ici ce qui me tiens à coeur :</p>

<h2>Le Jeu</h2>

<p>S'amuser, c'est aussi une excellente façon de faire connaissance :)</p>

<h3>Le jeu de rôle</h3>

<p>Le jeu de rôle, c'est une façon d'inventer des histoires.</p>

<h3>le jeu de GO</h3>

<p>Le jeu de GO nous viens de loin.</p>

<p>Au propre comme au figuré :</p>

<ul>
	<li>de Chine,</li>
	<li>d'il y a plus de 5000 ans</li>
</ul>

<p>&nbsp;</p>

<p>&nbsp;</p>

<ul>
	<li>&nbsp;</li>
	<li>&nbsp;</li>
</ul>

<h3>Le jeu en ligne</h3>

<p>Exclusivement des jeux Libres et Gratuits, multi-joueurs, pour passer de bons moments dans la convivialité.</p>

<h4>Globulation2</h4>

<p><img alt="Image flottante" class="imageflottante" src="http://dtar.fr/img/glob2-icon-48x48.png" /> Aaah, voilà un jeu absolument génial ! Globulation 2 est un jeu de Stratégie Temps Réel (RTS Real-Time Strategy) innovant qui réduit la gestion du joueur en assignant automatiquement des tâches aux unités. Et bien que ce soit aussi un jeu de conquête, il est complètement adapté aux plus jeunes, je trouve.</p>

<h4>Landes Éternelles</h4>

<p><img alt="Image flottante" class="imageflottante" src="http://dtar.fr/img/icon-le.bmp" /> Un jeu véritablement fascinant en 3D. On y incarne un personnage qui évolue au gré de nos envies... Carrément du jeu de rôle avec support graphique.</p>

<h4>The Mana World</h4>

<p><img alt="Image flottante" class="imageflottante" src="http://dtar.fr/img/Tmw_logo-mini.png" /> Un petit jeu très mignon en 2D, sur lequel on peut passer des heures, là aussi pour faire évoluer un personnage.</p>

<h4>Open TTD</h4>

<p><img alt="Image flottante" class="imageflottante" src="http://dtar.fr/img/openttd.32.bmp" /> On change de registre, pour construire un réseau de transport. Chemin de fer, bateaux, et transport routier, serez-vous capable de faire prospérer votre petite société ? Vous pourrez même faire grandir des villes, créer des industries, et construire des gares et des réseaux de transports hallucinants.</p>

<h4>FreeCiv</h4>

<p><img alt="Image flottante" class="imageflottante" src="http://dtar.fr/img/civicon.png" /> Vous démarrez avec un petit colon et un explorateur, pour créer une civilisation capable d'aller dans les étoiles... Avec FreeCiv, vous faites évoluer une civilisation de 4000 ans avant J.C. jusque dans le futur.</p>

<h4>FreeCol</h4>

<p><img alt="Image flottante" class="imageflottante" src="http://dtar.fr/img/freecol-mini.png" /> Petit frère de FreeCiv, sur le continent des Amériques. Vous incarnez une civilisation européenne en 1360 qui découvre et colonise le nouveau monde, pour finalement s'émanciper de sa métropole.</p>

<h2>La Décroissance</h2>

<p>Des idées pour vivre en harmonie et dans le <strong>respect de la planète, des animaux, des autres et de soi-même</strong>.</p>

<p><img alt="Image flottante" class="imageflottante" src="http://dtar.fr/img/velo.png" /> Voici un vélo, qui représente pour moi à l'heure actuelle la meilleure façon de se déplacer. On profite du bon air, et on ne polue pas. Argument non négligeable, c'est très économique comme mode de déplacement !</p>

<h2>Le Reïki</h2>

<p>Tu vas découvrir mon activité <strong>Reïki</strong></p>