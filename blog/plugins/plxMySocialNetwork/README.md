# Plugin plxMySocialNetwork pour PluXml
plxMySocialNetwork (fork de aplxSocialImg) : partager vos articles sur les réseaux sociaux

![demo](https://cloud.githubusercontent.com/assets/13441278/10242180/527d3188-68ef-11e5-9ae4-cd4ebb8be4d2.png)



## Fonctionnalités

* Affiche un lien pour partager un article sur les réseaux sociaux
* Réseaux sociaux: Facebook, Twitter, Google+, LinkedIn, Diaspora et Courriel
* Ajouter d'autres liens vers des réseaux sociaux
* Partager les articles et/ou les pages statiques
* Affichage des liens en texte ou boutons via font-awesome (à valider dans les paramètres)
* Pas de code tiers


## Insérer plxMySocialNetwork le thème utilisé

Dans le fichier article.php,article-full-width.php et/ou static.php, static-full-width.php de votre thème, ajoutez la ligne suivante à l'endroit où vous souhaitez afficher les boutons.


    <?php eval($plxShow->callHook('MySocialNetwork')) ?>


## Modifier le CSS

Paramètres > Plugins > menu "Plugins actifs" > plugin "MySocialNetwork" > menu "Code css" > champ "Contenu fichier css site"


![selection_014](https://cloud.githubusercontent.com/assets/13441278/10366062/12e7717c-6dc8-11e5-8dd1-76bc9315ab52.png)
![selection_013](https://cloud.githubusercontent.com/assets/13441278/10366064/12f63306-6dc8-11e5-9e79-70a92413fe74.png)
![selection_012](https://cloud.githubusercontent.com/assets/13441278/10366065/12f729d2-6dc8-11e5-8987-a9861a04adf0.png)
![selection_003](https://cloud.githubusercontent.com/assets/13441278/10366061/12d61300-6dc8-11e5-80d1-b657e5b4ba16.png)
![selection_011](https://cloud.githubusercontent.com/assets/13441278/10366066/12f78c06-6dc8-11e5-93ca-8e444e8cb6c2.png)
![selection_009](https://cloud.githubusercontent.com/assets/13441278/10366063/12f584ba-6dc8-11e5-9820-4238538004e4.png)


  
## Evolutions possible

* modifier l'ordre des liens

Le plugin complet est disponible sur  mon [dépôt de plugins pour Pluxml](http://blog.niqnutn.com/plugins/repository/index.php) ou sur [Github](https://github.com/nIQnutn/plxMySocialNetwork)   
