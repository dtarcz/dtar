<?php
/**
 * Classe plxStats 
 *
 * @version 1.0
 * @package PLX
 * @author    Stephane F
 **/
class plxStats {

    var $filename = null; # nom du fichier contenant les statistiques
    var $aStats = array(); # données statistiques
    var $sessionTime = 15; # durée d'une visite en minute vant expiration

    /**
     * Constructeur qui initialise certaines variables de classe
     * et qui lance les traitements de prise en compte des statistiques
     *
     * @param    null
     * @return    null
     * @author    Stephane F
     **/    
    function plxStats() {
    
        $this->filename = PLX_ROOT.'plugins/stats/stats.xml'; # emplacement du fichier des stats
    
        $this->_getStatiques($this->filename); # récuperation des stats
        $this->_cleanVisits(); # suppression des anciens visiteurs 
        $this->_addVisitor(); # ajoute le nouveau visiteur et gère les compteurs
        $this->_editStats($this->filename); # sauvegarde des stats
    }
    
    #------------------
    # Méthodes privées
    #------------------
    
    /**
     * Méthode qui parse le fichier des statistiques et qui alimente
     * le tableau aStats
     *
     * @param    null
     * @return    null
     * @author    Stephane F
     **/
    function _getStatiques() {

        if(!is_readable($this->filename)) return;
            
        # Mise en place du parseur XML
        $data = implode('',file($this->filename));
        $parser = xml_parser_create(PLX_CHARSET);
        xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
        xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,0);
        xml_parse_into_struct($parser,$data,$values,$iTags);
        xml_parser_free($parser);
        # Recuperation des valeurs de nos champs XML
        $this->aStats['views_today'] = array( // total de pages vues aujourd'hui
                'date'         => (isset($values[ $iTags['views_today'][0] ]['attributes']['date'])?$values[ $iTags['views_today'][0] ]['attributes']['date']:date('Ymd')),
                'counter'    => (isset($values[ $iTags['views_today'][0] ]['value'])?intval($values[ $iTags['views_today'][0] ]['value']):0)
            );
        $this->aStats['views_total'] = isset($values[ $iTags['views_total'][0] ]['value'])?intval($values[ $iTags['views_total'][0] ]['value']):0; // total de pages vues
        $this->aStats['visitors'] = isset($values[ $iTags['visitors'][0] ]['value'])?intval($values[ $iTags['visitors'][0] ]['value']):0; // nombre total de visiteurs 
        
        # On verifie qu'il existe des tags "online"
        if(isset($iTags['online'])) {
            # On compte le nombre de tags "online"
            $nb = sizeof($iTags['online']);
            # On boucle sur $nb
            for($i = 0; $i < $nb; $i++) {
                $this->aStats['online'][ $values[ $iTags['online'][$i] ]['attributes']['ip'] ] = $values[ $iTags['online'][$i] ]['value'];
            }
        }
    }
    
    /**
     * Méthode qui sauvegarde les statistiques dans le fichier xml
     * le tableau aStats
     *
     * @param    null
     * @return    null
     * @author    Stephane F
     **/    
    function _editStats() {

        # Début du fichier XML
        $xml = "<?xml version='1.0' encoding='".PLX_CHARSET."'?>\n";
        $xml .= "<document>\n";
        $xml .= "\t<views_today date=\"".$this->aStats['views_today']['date']."\">".$this->aStats['views_today']['counter']."</views_today>\n";        
        $xml .= "\t<views_total>".$this->aStats['views_total']."</views_total>\n";
        $xml .= "\t<visitors>".$this->aStats['visitors']."</visitors>\n";
        if ($this->aStats['online']) {
            foreach($this->aStats['online'] as $ip => $time) {
                $xml .= "\t<online ip=\"".$ip."\">".$time."</online>\n";
            }
        }
        $xml .= "</document>";
        # Sauvegarde du fichier
        plxUtils::write($xml, $this->filename);
    }
    
    /**
     * Méthode qui supprime les anciens visiteurs si la durée de la visite
     * est expirée
     *
     * @param    null
     * @return    null
     * @author    Stephane F
     **/    
    function _cleanVisits() {
    
        if ($this->aStats['online']) {
            foreach($this->aStats['online'] as $ip => $time) {
                if(time() - $time >= $this->sessionTime * 60)
                    unset($this->aStats['online'][$ip]); # suppresion de l'ancien visiteur
            }
        }
        
    }
    
    /**
     * Méthode qui ajoute un nouveau visiteur et qui incrémente les différents
     * compteurs de statistiques
     *
     * @param    null
     * @return    null
     * @author    Stephane F
     **/    
    function _addVisitor() {
    
        if (isset($this->aStats['online'][plxUtils::getIp()])) {
            if (time() - $this->aStats['online'][plxUtils::getIp()] >= $this->sessionTime * 60) {
                $this->aStats['online'][plxUtils::getIp()] = time(); 
                $this->aStats['visitors']++; 
            }
        }
        else {
            $this->aStats['online'][plxUtils::getIp()] = time(); 
            $this->aStats['visitors']++; 
        }

        # nombre de visiteurs du jour
        if ($this->aStats['views_today']['date'] == date('Ymd'))
            $this->aStats['views_today']['counter']++;
        else {
            $this->aStats['views_today']['date'] = date('Ymd');
            $this->aStats['views_today']['counter']=1;
        }
        
        # nombre total de pages vues
        $this->aStats['views_total']++; 

    }
    
    #-------------------
    # Méthodes publiques
    #-------------------
    
    /**
     * Méthode qui affiche le compteur du nombre total de visiteurs
     *
     * @param    null
     * @return    stdout
     * @author    Stephane F
     **/    
    function TotalVisitors() {
        echo $this->aStats['visitors'];
    }
    
    /**
     * Méthode qui affiche le compteur du visiteurs en ligne
     *
     * @param    null
     * @return    stdout
     * @author    Stephane F
     **/    
    function OnlineVisitors() {
      global $nbtemp;
        echo sizeof($this->aStats['online']);
    }
	
	 /**
     * Ecris 's' s'il y a plusieurs visiteurs
     *
     * @param    null
     * @return    stdout
     * @author    Ludo_17 et Amaury G
     **/    
    function Pluriel() {
        if(sizeof($this->aStats['online']) > 1)
			echo 's';
    }
	    
    /**
     * Méthode qui affiche le compteur du nombre de pages vues aujourd'hui
     *
     * @param    null
     * @return    stdout
     * @author    Stephane F
     **/
    function TodayViews() {
        echo $this->aStats['views_today']['counter'];
    }
    
    /**
     * Méthode qui affiche le compteur du nombre total de pages vues
     *
     * @param    null
     * @return    stdout
     * @author    Stephane F
     **/    
    function TotalViews() {
        echo $this->aStats['views_total'];
    }    
}

#-----------------------------------------------
# création d'une instance de la classe plxStats
#-----------------------------------------------
$plxStats = new plxStats();
?>