<?php include(dirname(__FILE__).'/header.php'); ?>

	<div id="section">

		<div id="article">

				<h2><?php $plxShow->artTitle(''); ?></h2>
				<div class="infosnews">
          <p class="art-topinfos"><?php $plxShow->lang('WRITTEN_BY') ?> <?php $plxShow->artAuthor() ?> - <span class="date"><?php $plxShow->artDate('#num_day #month #num_year(4) #hour:#minute'); ?></span> &#9733; <span class="commentaires"><?php $plxShow->artNbCom(); ?></span></p>
				  <p class="art-infos"><?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat(); ?> - <span class="tag"><?php $plxShow->lang('TAGS') ?></span> : <?php $plxShow->artTags(); ?></p>
				
          <?php $plxShow->artAuthorInfos('<div class="author-infos">#art_authorinfos</div>'); ?>
				
        </div>
                
        <div class="art-chapo"><?php $plxShow->artContent(); ?></div>
        
        <?php include(dirname(__FILE__).'/commentaires.php'); ?>
             

    </div>

		<?php include(dirname(__FILE__).'/sidebar.php'); ?>

	</div>

<?php include(dirname(__FILE__).'/footer.php'); ?>     

