<?php if(!defined('PLX_ROOT')) exit; ?>

	<div id="aside">

		<h5><span><?php $plxShow->lang('CATEGORIES') ?></span></h5>
		<ul>
			<?php $plxShow->catList('','<li id="#cat_id" class="#cat_status"><a href="#cat_url" title="#cat_name">#cat_name</a><span class="nbr">#art_nb</span></li>'); ?>
		</ul>

		<h5><span><?php $plxShow->lang('ARCHIVES') ?></span></h5>
        <ul>
            <?php $plxShow->archList('<li id="#archives_id" class="#archives_status"><a href="#archives_url" title="#archives_name">#archives_name</a><span class="nbr">#archives_nbart</span></li>'); ?>
        </ul>

      <div id="side-tags" class="tagon">
        <h5><span><?php $plxShow->lang('TAGS') ?></span></h5>
		      <ul>
			       <?php $plxShow->tagList('<a href="#tag_url" title="#tag_name">#tag_name</a> &nbsp; ', 8); ?>
		      </ul>  
      </div>
        
		<h5><span><?php $plxShow->lang('LAST_ARTICLES') ?></span></h5>
		<ul>
			<?php $plxShow->lastArtList('<li class="#art_status"><a href="#art_url" title="#art_title">#art_title</a></li>'); ?>
		</ul>

		<h5><span><?php $plxShow->lang('LAST_COMMENTS') ?></span></h5>
		<ul>
			<?php $plxShow->lastComList('<li><a href="#com_url">#com_author '.$plxShow->getLang('SAID').' : #com_content(34)</a></li>'); ?>
		</ul>

	</div>    
