<?php include(dirname(__FILE__).'/header.php'); ?>

    <!-- Carousel -->
    <div id="myCarousel" class="carousel slide">
      <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <div class="item active">          
            <div class="fill" style="background-image:url('http://placehold.it/1900x1080&text=Image 1');"></div>
            <div class="carousel-caption">
              <h1>Bootstrap 3 Template pour PluXml  <i class="icon-leaf"></i></h1>
            </div>
          </div>
          <div class="item">
            <div class="fill" style="background-image:url('http://placehold.it/1900x1080&text=Image 2');"></div>
            <div class="carousel-caption">
              <h1>Modifier ce contenu dans le template  <em>static-home.php</em> de votre thème</h1>
            </div>
          </div>
          <div class="item">
            <div class="fill" style="background-image:url('http://placehold.it/1900x1080&text=Image 3');"></div>
            <div class="carousel-caption">
              <h1>100% compatible avec <a href="http://pluxml.org">PluXml 5.2</a></h1>
              <p><a class="btn btn-large btn-primary" href="http://my.pluxml.free.fr/download.php?file=Bootstrap3.zip">TÉLÉCHARGER BOOTSTRAP 3</a></p>
            </div>
          </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="icon-next"></span>
        </a>
    </div>
    <!-- /Carousel -->

    <div class="section">

      <div class="container">

        <div class="row">
        
          <div class="col-lg-4 col-md-4">
            <img style="margin-left: 80px" class="img-circle" src="<?php $plxShow->template(); ?>/slider/pic.png" alt="Generic placeholder image">
            <h3><i class="icon-ok-circle"></i> Réalisé avec Bootstrap 3</h3>
            <p>Réalisé a partir du Framework <a href="http://getbootstrap.com">Bootstrap 3</a>.<br />
               Soyez sur d'utiliser la dernière version !
            </p>
          </div>
          
          <div class="col-lg-4 col-md-4">
            <img style="margin-left: 80px" class="img-circle" src="<?php $plxShow->template(); ?>/slider/pic.png" alt="Generic placeholder image">
            <h3><i class="icon-pencil"></i> Facile d'utilisation</h3>
            <p>Vous pouvez modifier sa couleur en récupérant les feuilles de style sur le site <a href="http://bootswatch.com">Bootswatch</a>, ou en composant vous même <a href="http://getbootstrap.com/customize/">l'interface de Bootstrap</a> sur le site officiel sous forme de package prêt à l'emploi !</p>
          </div>
          
          <div class="col-lg-4 col-md-4">
            <img style="margin-left: 80px" class="img-circle" src="<?php $plxShow->template(); ?>/slider/pic.png" alt="Generic placeholder image">
            <h3><i class="icon-folder-open-alt"></i> Quelques pages fournies</h3>
            <p>Vous trouverez d'autre modèles de pages pour PluXml au fir et à mesure des mise à jour.<br />
            Actuellement il est 100% compatible avec la version 5.2 de PluXml.
            </p>
          </div>
          
        </div><!-- /.row -->

      </div><!-- /.container -->

    </div><!-- /.section -->

   <div class="container">
        <div class="row">
         <div class="col-lg-12">
          <?php $plxShow->staticContent(); ?>
         </div>
        </div>
   </div>
   
<?php include(dirname(__FILE__).'/footer.php'); ?>