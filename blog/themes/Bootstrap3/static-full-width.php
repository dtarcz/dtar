<?php include(dirname(__FILE__).'/header.php'); ?>
    <div class="container">
    
      <div class="row">

        <div class="col-lg-12">
        
          <div id="static-<?php echo $plxShow->staticId(); ?>">
              <h1><?php $plxShow->staticTitle(); ?></h1>
              <hr>
              <?php $plxShow->staticContent(); ?>
          </div>

        </div><!-- /col-lg-12 -->

      </div><!-- /row -->
      
    </div><!-- /.container -->
<?php include(dirname(__FILE__).'/footer.php'); ?>
