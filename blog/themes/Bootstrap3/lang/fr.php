<?php

$LANG = array(

#header.php
'HOME'					=> 'Accueil',
'BLOG'					=> 'Blog',
'GOTO_CONTENT'			=> 'Aller au contenu',
'GOTO_MENU'				=> 'Aller au menu',
'COMMENTS_RSS_FEEDS'	=> 'Fil Rss des commentaires',
'COMMENTS'				=> 'Fil des commentaires',
'ARTICLES_RSS_FEEDS'	=> 'Fil Rss des articles',
'ARTICLES'				=> 'Fil des articles',

# sidebar.php
'CATEGORIES' 			=> 'Catégories',
'LATEST_ARTICLES'		=> 'Derniers articles',
'LATEST_COMMENTS'		=> 'Derniers commentaires',
'ARCHIVES'				=> 'Archives',

# footer.php
'POWERED_BY'			=> 'Généré par',
'DESIGN_BY'             => '<a href="http://forum.pluxml.org/profile.php?id=2985"><i class="icon-magic"></i> Réalisé par Fred</a>',
'PLUXML_DESCRIPTION'	=> 'Blog ou Cms sans base de données',
'IN'					=> 'en',
'ADMINISTRATION'		=> 'Administration',
'GOTO_TOP'				=> 'Remonter en haut de page',
'TOP'					=> 'Haut de page',

# erreur.php
'ERROR'					=> 'La page que vous avez demandé n\'existe pas',
'BACKTO_HOME'			=> 'Retour à la page d\'accueil',

# common
'WRITTEN_BY'			=> 'Rédigé par',
'CLASSIFIED_IN'			=> 'Classé dans',
'TAGS'					=> 'Mots clés',

# commentaires.php
'SAID'					=> 'a dit',
'WRITE_A_COMMENT'		=> 'Écrire un commentaire',
'NAME'					=> 'Nom',
'WEBSITE'				=> 'Site (facultatif)',
'EMAIL'					=> 'Adresse e-mail',
'COMMENT'				=> 'Contenu de votre message',
'CLEAR'					=> 'Effacer',
'SEND'					=> 'Envoyer votre commentaire',
'COMMENTS_CLOSED'		=> 'Les commentaires sont fermés',
'ANTISPAM_WARNING'		=> 'Vérification anti-spam',
);

?>
