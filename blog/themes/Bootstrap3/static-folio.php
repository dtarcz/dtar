<?php include(dirname(__FILE__).'/header.php'); ?>
    <div class="container">
    
      <div class="row">
        <div class="col-lg-12">       
          <div id="static-<?php echo $plxShow->staticId(); ?>">
              <h1><?php $plxShow->staticTitle(); ?></h1>
              <hr>
              <?php $plxShow->staticContent(); ?>
          </div>
        </div><!-- /col-lg-12 -->           
      </div><!-- /row -->   

    
        
        <?php $plxShow->lastArtList($format='
      <div class="row">
        <div class="col-sm-6 col-md-3">
          <div class="thumbnail">
            #art_chapo
            <div class="caption">
              <h3>#art_title</h3>
              <p>#art_content(95)</p>
              <p><a href="#art_url" class="btn btn-primary"><i class="icon-eye-open"></i> Détail/Achat</a> <a href="#art_url" class="btn btn-default"><i class="icon-comments"></i> Avis <span class="label label-info">#art_nbcoms</span></a></p>
            </div>
          </div>
        </div>',8,002,''); ?>            
      
    </div><!-- /.container -->
<?php include(dirname(__FILE__).'/footer.php'); ?>
