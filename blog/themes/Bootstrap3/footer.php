<?php if (!defined('PLX_ROOT')) exit; ?>

    <div class="container">

      <hr>

      <footer>
        <div class="row">
          <div class="col-lg-12">
          
          <ul class="list-unstyled list-inline  pull-left">
              <li><?php $plxShow->mainTitle('link'); ?> © 2015 - <?php $plxShow->subTitle(); ?></li>	                  
		      <li><?php $plxShow->lang('POWERED_BY') ?> <a href="http://www.pluxml.org" title="<?php $plxShow->lang('PLUXML_DESCRIPTION') ?>">PluXml</a>
		      <?php $plxShow->lang('IN') ?> <?php $plxShow->chrono(); ?> <?php $plxShow->httpEncoding() ?></li>
		      <li><?php $plxShow->lang('DESIGN_BY') ?></li>
		      <li class="tooltip-social"><a rel="nofollow" href="<?php $plxShow->urlRewrite('core/admin/'); ?>" data-toggle="tooltip" data-placement="top" title="<?php $plxShow->lang('ADMINISTRATION') ?>"><i class="icon-lock"></i></a></li>
          </ul>
          
          <ul class="list-unstyled list-inline list-social-icons pull-right">
              <li class="tooltip-social facebook-link"><a href="#facebook-page" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="icon-facebook-sign icon-2x"></i></a></li>
              <li class="tooltip-social linkedin-link"><a href="#linkedin-company-page" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="icon-linkedin-sign icon-2x"></i></a></li>
              <li class="tooltip-social twitter-link"><a href="#twitter-profile" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="icon-twitter-sign icon-2x"></i></a></li>
              <li class="tooltip-social google-plus-link"><a href="#google-plus-page" data-toggle="tooltip" data-placement="top" title="Google+"><i class="icon-google-plus-sign icon-2x"></i></a></li>
              <li class="tooltip-social"><a style="font-size: 30px" data-toggle="tooltip" data-placement="top" href="<?php echo $plxShow->urlRewrite('#top') ?>" title="<?php $plxShow->lang('GOTO_TOP') ?>"><i class="icon-chevron-sign-up"></i></a></li>
          </ul> 
           
          </div>
        </div>
      </footer>

    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php $plxShow->template(); ?>/js/jquery.js"></script>
    <script src="<?php $plxShow->template(); ?>/js/bootstrap.js"></script>
    <script>
    // Tooltips
    $('.tooltip-social').tooltip({
      selector: "a[data-toggle=tooltip]"
    });
    // Carousel
    $('.carousel').carousel({
       interval: 3000
    });      	     
    </script>
  </body>
</html>
