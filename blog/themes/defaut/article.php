<?php include(dirname(__FILE__).'/header.php'); ?>

	<main class="main grid" role="main">

		<section class="col sml-12 med-8">

			<article class="article" role="article" id="post-<?php echo $plxShow->artId(); ?>">

				<header>
					<h1>
						<?php $plxShow->artTitle(); ?>
					</h1>
					<small>
						<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?> -
						<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>"><?php $plxShow->artDate('#num_day #month #num_year(4)'); ?></time> -
						<a href="#comments" title="<?php $plxShow->artNbCom(); ?>"><?php $plxShow->artNbCom(); ?></a>
					</small>
				</header>

				<section>
					<?php $plxShow->artContent(); ?>
				</section>

				<footer>
					<small>
						<?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat() ?> - 
						<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
					</small>
				</footer>

			</article>

			<?php $plxShow->artAuthorInfos('<div class="author-infos">#art_authorinfos</div>'); ?>
			
			<?php
			# paramétrage
			$nb_arts = 3; 
			$format='<li>#art_date : <a href="#art_url" title="#art_title">#art_title</a></li>';
			# recherche des catégories actives de l'article en cours de lecture
			$cat_ids = $plxShow->artActiveCatIds();
			$cat_ids = ($cat_ids ? implode('|', $cat_ids) : '');
			# recherche de tous les articles publiés dans les catégories actives de l'article en cours de lecture
			$plxGlob_arts = clone $plxMotor->plxGlob_arts;
			$motif = '/^[0-9]{4}.((?:[0-9]|home|,)*(?:'.str_pad($cat_ids,3,'0',STR_PAD_LEFT).')(?:[0-9]|home|,)*).[0-9]{3}.[0-9]{12}.[a-z0-9-]+.xml$/';
			$aFiles = $plxGlob_arts->query($motif,'art','rsort',0,9999,'before');
			if($aFiles and sizeof($aFiles)>1) {
				$arts = array();
				# recherche aléatoire des articles à recommander
				$random = array_rand($aFiles, ($nb_arts > sizeof($aFiles) ? sizeof($aFiles) : $nb_arts) );
				foreach($random as $numart) {
					# on ne liste pas l'article en cours de lecture 
					if($aFiles[$numart] <> basename($plxMotor->plxRecord_arts->f('filename'))) {
						$art = $plxMotor->parseArticle(PLX_ROOT.$plxMotor->aConf['racine_articles'].$aFiles[$numart]);
						$row = str_replace('#art_url',$plxMotor->urlRewrite('?article'.intval($art['numero']).'/'.$art['url']),$format);	
						$row = str_replace('#art_title',plxUtils::strCheck($art['title']),$row);	
						$row = str_replace('#art_date',plxDate::formatDate($art['date'],'#num_day/#num_month/#num_year(4)'),$row);	
						$author = plxUtils::getValue($plxMotor->aUsers[$art['author']]['name']);
						$row = str_replace('#art_author',plxUtils::strCheck($author),$row);
						$arts[] = $row;		
					}
				}
				# affichage des résultats
				if($arts) {
					echo '<h2>À lire également</h2>';
					echo '<ul>'.implode('', $arts).'</ul>';
				}
			}
			?>


			<?php include(dirname(__FILE__).'/commentaires.php'); ?>

		</section>

		<?php include(dirname(__FILE__).'/sidebar.php'); ?>

	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
