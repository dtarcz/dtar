<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">


	<div class="element_menu"> <!-- Cadre correspondant à un sous-menu -->
		<h3>Visite virtuelle</h3> <!-- Titre du sous-menu -->
			<ul>
				<li><a href="#.php">Couloir d'entrée.</a></li>
				<li><span class="texte_menu">Pièce à vivre.</li> <!-- Liste des liens du sous-menu -->
					<div class="sous_element_menu">
						<ul>
							<li><a href="#.php">Coin cuisine.</a></li>
							<li><a href="#.php">Coin salon.</a></li>
							<li><a href="#.php">Vue du balcon.</a></li>
						</ul>
					</div>
				<li><a href="#.php">Chambre.</a></li>
				<li><a href="#.php">Coin toilettes.</a></li>
				<li><a href="#.php">Parkings.</a></li>
				<li><a href="#.php">Carte.</a></li>
			</ul>

	</div>
	
	<div class="element_menu"> <!-- Cadre correspondant à un sous-menu -->
		<h3>Prendre contact</h3> <!-- Titre du sous-menu -->
			<ul>
				<li><a href="agenda.php">Disponibilités.</a></li>
				<li><a href="contact.php">Contact.</a></li>
			</ul>
	</div>
			
	<div class="element_menu"> <!-- Cadre correspondant à un sous-menu -->
		<h3>Partenaires</h3> <!-- Titre du sous-menu -->
			<ul>
				<li><a href="partenaires.php">CleVacances.</a></li>
			</ul>
	</div>
