<?php
			include("en-tete.php");
			?>
		</div>
 
		<div id="menu">
			<?php // Inclusion du menu:
			include("menu.php"); 
			?>			
		</div>
 
		<div id="corps">
			<p>
			<h1>LA CHARTE
			<br />
			"ESPRIT DU SEL"</h1>

				<h3>Le Système d'Échange Local qui se réfère à la présente charte dite « Esprit du SEL » :</h3>
					<ul>
						<li><p>c'est échanger autrement et librement en unité du SEL local ; </p></li>
						<li><p>c'est privilégier le lien plutôt que le bien ; </p></li>
						<li><p>c'est valoriser les savoirs, les savoir-faire et la responsabilité de chacun par la coopération, la solidarité et la réciprocité multilatérale.</p></li>
					</ul>
					<br />

				<h3>Le Système d'échange local qui se réfère à la présente charte dite « Esprit du SEL », exprime sa volonté de :</h3>
					<ul>
						<li><p>expérimenter et développer des pratiques d'échanges estimés en unités locales, sans argent, de manière loyale et équitable, au sein d'un territoire de proximité ou sous forme d'intersel régional ou national ;</p></li>
						<li><p>faire vivre des valeurs solidaires fondées sur le dialogue, la confiance, la convivialité, le lien social et la réciprocité au sein du groupe ;</p></li>
						<li><p>fonctionner de manière démocratique et participative au moyen de structures transparentes et autogérées, en toute indépendance vis-à-vis des partis politiques et de mouvements religieux ou idéologiquement exclusifs ;</p></li>
						<li><p>oeuvrer dans le respect des équilibres naturels tout en favorisant l'épanouissement des participants, dans le respect du rythme personnel de chacun.</p></li>
					</ul>
					<br />
 				
				<h3>Chaque SEL est autonome et s'engage à respecter les autres SEL et la richesse de leurs différences.</h3>
			</p>
		</div>
 
		<div id="pied_de_page">
			<?php // Inclusion du pied de page:
			include("pied.php"); 
			?>
		</div>
