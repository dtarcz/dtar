<div class="element_menu"> <!-- Cadre correspondant au sous-menu element, un "." dans le css -->
	<h3>Contact :</h3> <!-- Titre du sous-menu -->
		<ul>
			<li><a href="mailto:universel11100@gmail.com">Par Courriel</a></li>
			<br />
		</ul>
</div>
<div class="element_menu"> <!-- Cadre correspondant au sous-menu element, un "." dans le css -->
	<h3>Qui sommes nous ?</h3> <!-- Titre du sous-menu -->
		<ul>
			<li><a href="uvs11_statuts.php">Statuts</a></li>
			<br />
			<li><a href="uvs11_reglement.php">Règlement</a></li>
			<br />
			<li><a href="uvs11_charte.php">Charte</a></li>
			<br />
		</ul>

</div>
<div class="element_menu"> <!-- Cadre correspondant au sous-menu element, un "." dans le css -->
	<h3>Autre sites :</h3> <!-- Titre du sous-menu -->
		<ul>
			<li><a href="uvs11_partenaires.php">Partenaires</a></li>
			<br />
		</ul>
</div>
