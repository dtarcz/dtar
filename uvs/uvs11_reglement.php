<?php
			include("en-tete.php");
			?>
		</div>
 
		<div id="menu">
			<?php // Inclusion du menu:
			include("menu.php"); 
			?>			
		</div>
 
		<div id="corps">
			<p>
				<h1>REGLEMENT  INTERIEUR</h1>
				<br /></p>
				<p><h3>ARTICLE 1 : COMPOSITION DE L'ASSOCIATION</h3><br />
				Une fois par an, lors de l'Assemblée Générale, le Conseil d'Administration est élu par tous les adhérents. Ce Conseil mandate le bureau qui se compose d'un(e) Président(e), d'un(e) Vice-Président(e), d'un(e) Trésorier(e), d'un(e) Trésorier(e) adjoint(e) et d'un(e) Secrétaire. Le bureau prend toutes les décisions liées au bon fonctionnement de l'association.
				<br />
				</p>
				<p><h3>ARTICLE 2 : REPARTITION DES TACHES</h3><br />
				Les membres du bureau sont aidés dans leurs fonctions par différents comités : éthique, rédaction, accueil et permanence ainsi que divers postes : factrice, responsable de salles, coordinatrice d'informations, etc.
				<br /><br />
				</p>
				<p><h3>ARTICLE 3 : PRINCIPES DE L'ECHANGE</h3><br />
				A.	La  valeur de l'échange résulte d'un accord mutuel entre les 2 parties qui sont « co-responsables ».<br />
				B.	Nul n'est obligé d'accepter un échange qui lui est proposé.<br />
				C.	L'unité d'échange est « l'heure ». Celle-ci n'est pas transformable en Euros. Sa division minimale est par 5 minutes.<br />
				D.	Tous les échanges sans exception se font en minutes ou en heures. Il doit être convenu d'avance entre les parties des éventuels frais en Euros engagés par l'offrant, tels que carburant, matière première...<br />
				E.	L'association n'est pas responsable des échanges entre adhérents quant à la qualité, les conditions, la valeur, la garantie des éléments échangés ou des services proposés. L'association et ses adhérents n'ont pas obligation de répondre à des demandes spécifiques.<br />
				F.	Chaque adhérent garde toute sa responsabilité et s'entoure de toutes les garanties pour que son activité à l'intérieur du S.E.L. soit conforme à l'éthique du S.E.L.(voir la charte « Esprit du Sel ») et au Code du Travail (voir art.1.324, 9 0 14 et circulaire du 8/10/87). Les adhérents dont la profession est en rapport avec leurs offres, prennent leurs responsabilités vis-à-vis des services fiscaux et administratifs.<br />
				G.	L'association se réserve le droit de refuser de comptabiliser un échange non-conforme à «L'Esprit du Sel» (voir la Charte), au présent règlement intérieur ou à la réglementation.<br />
				H.	En cas de litige entre adhérents, le Comité d'Ethique est habilité à recevoir les doléances de chacun et propose une solution avec approbation du(de la) Président(e).<br />
				<br />
				</p>
				<p><h3>ARTICLE 4 : LE BULLETIN</h3><br />
				A.	Les adhérents fournissent une liste de biens, savoirs et services, qu'ils désirent offrir ou demander. Un catalogue édité par l'association répertorie les différentes offres et demandes.
				<br />
				B.	Parallèlement à ce bulletin, un répertoire des adhérents, comportant leurs coordonnées, est diffusé auprès des adhérents et réservé à eux seuls. Ils s'engagent à ne pas le diffuser aux non adhérents.
				<br />
				C.	Le bulletin est édité tous les mois pairs sauf en août.
				<br />
				<br />
				</p>
				<p><h3>ARTICLE 5 : MODALITES DE L'ECHANGE</h3><br />
				A.	Chaque nouvel adhérent ou ré-adhérent reçoit une feuille annuelle d'échanges, comportant son N° d'adhérent et son solde en heures.
				<br />B.	Lors d'un échange, le compte de celui qui offre est augmenté de la valeur de l'échange et le compte de celui qui demande est diminué de la même valeur. Chaque adhérent indique son solde après échange sur la feuille du partenaire et la signe pour approbation.
				<br />C.	Afin de faciliter la transparence mutuelle, chaque adhérent accepte que le solde de son compte soit publié et communiqué à la demande d'un autre adhérent 
				<br />D.	Pour le bon fonctionnement de notre SEL et afin de contribuer au dédommagement des personnes rendant services à l'association, chaque adhérent (ou ré-adhérent) s'engage à donner 1 heure de son compte chaque année.
				<br />E.	Le débit et le crédit maximum autorisés sont de plus 50 heures ou moins 50 heures. En cas de dépassement, tout adhérent se doit d'ajuster son compte avant de poursuivre de nouveaux échanges avec d'autres adhérents.
				<br />F.	En fin d'année, les feuilles d'échanges annuelles doivent être impérativement restituées au(à la) Trésorier(e) qui lui en fournira une nouvelle pour l'année suivante si l'adhésion est renouvelée.
				En cas de non ré-adhésion, les feuilles d'échanges annuelles doivent être rendues.
				<br />G.	Chaque adhérent s'engage à mettre son compte à zéro en quittant l'association ou peut transmettre son solde au profit d'un autre adhérent de son choix.
				<br />H.	Toute information sur la nature des échanges est confidentielle. Le SEL ne peut cependant garantir cette confidentialité, ni être tenu pour responsable d'une violation.
				<br /><br />
				</p>
				<p><h3>ARTICLE 6 : ASSURANCES</h3><br />
				A.	Le SEL est assuré pour toutes les réunions en salle entre sélistes et les sorties en extérieur organisées par le SEL.
				<br />B.	Pour les échanges entre sélistes à domicile, chaque adhérent doit avoir souscrit une assurance responsabilité civile qui le couvrira. En cas de non-souscription, le SEL de Narbonne « UNIS VERS SEL » se dégage de toute responsabilité.
				</p>
		</div>
 
		<div id="pied_de_page">
			<?php // Inclusion du pied de page:
			include("pied.php"); 
			?>
		</div>
