
<?php include("en-tete.php"); ?>
		</div>
 
		<div id="menu">
			<?php // Inclusion du menu:
			include("menu.php"); 
			?>			
		</div>
 
		<div id="corps">
		
			<p><h1>Unis Vers SEL:</h1>
			<h2>le Système d'Échange Local (SEL) du Narbonnais.</h2>
			<br />
			<center>LE LIEN EST PLUS IMPORTANT QUE LE BIEN</center>
			
			<blockquote>"Un Système d'Echange Local (ou SEL) est un système d'échange alternatif, construit à côté du système dominant d'économie de marché.<br />Les SEL sont des associations déclarées ou de fait à but non lucratif, implantés localement, et qui permettent à leurs membres de procéder à des échanges de biens, de services et de savoirs sans avoir recours à la monnaie traditionnelle."<br /><em>Wikipedia</em></blockquote>
				<br />
				<br /></p>
				<div id="index"><b>
				Les échanges sont mesurés dans une unité choisie par chaque SEL. A Narbonne ce sont les "heures". Une heure de service vaut une autre heure. <br />Tout d'abord cette unité de mesure est virtuelle et n'apparaît que sur la feuille d'échange des adhérents (de moins 50 heures à plus 50 heures). <br />Ensuite elle n'a d'intérêt que pour échanger avec les autres membres du SEL et ne vaut rien en dehors des SEL. <br />Dans le SEL, on échange avec différentes personnes à divers moments, des biens ou des services de valeurs différentes. La réciprocité immédiate n'étant pas obligatoire les échanges sont plus variés, plus nombreux, plus libres et plus conviviaux.
				<br />
				<h3>Convivialité, Réseau, Services, Lien, Soins, Bien-être</h3>
				<center>
				<img alt="Repas partagé du dim. 4 juillet 2010" title="Repas partagé du dim. 4 juillet 2010" src="img/repas-partagé-juillet-2010-bis.jpg"/>
				</center>	
				</b>
				</div>
				<br />				
				<br />
				<br />
		</div>
 
		<div id="pied_de_page">
			<?php // Inclusion du pied de page:
			include("pied.php"); 
			?>
		</div>
