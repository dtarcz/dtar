<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" >

	<head>
		<!-- Titre de la page -->
		<title>Unis Vers Sel 11 - Système d'échange Local de la Narbonnaise - association loi 1901 d'échanges de biens, de services et de savoirs</title>
		<!-- Table de caractères -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<!-- Description de la page -->
		<meta name="description" content="Un Système d'Echange Local (ou SEL) est un système d'échange alternatif. Unis Vers Sel 11, association loi 1901, le SEL de la Narbonnaise, permet a ses membres de procéder à des échanges de biens, de services et de savoirs sans avoir recours à la monnaie traditionnelle. Convivialité, Réseau, Services, Lien, Soins, Bien-être" />
		<!-- Mots-clefs -->		
		<meta name="keywords" content="sel de l'Aude, sel de la Narbonnaise, systeme d'echange local, SYSTEME D'ECHANGE LOCAL, SEL, echange, association loi 1901, Convivialite, Reseau, Services, Lien, Soins, Bien-etre, universel, asso, universel11, unis vers sel, UNIS VERS SEL" />
		<!-- Commandes robots -->
		<meta name="robots" content="index, follow" />
		<!-- Designation de la feuille de style de la page -->
		<link rel="stylesheet" media="screen" type="text/css" title="Unis Vers Sel 11" href="style_01.css" />
		<!-- Feuille de style alternative -->
		<link rel="stylesheet" media="screen" type="text/css" title="Design Sombre-2" href="style_dark.css" />
		<!-- Affichage d'une favicone -->
		<link rel="icon" type="image/png" href="img/favicon.png" />
		<!-- En validant votre site, vous confirmez auprès de Google que vous en êtes bien le propriétaire. -->
		<meta name="google-site-verification" content="U54SYDAX4PzbyAuphEV8cPOvSruFpPLjJchz_y3ZESY" />
	</head>
		<!-- DEBUT SCRIT GOOGLE ANALYTIC -->
		<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
		try {
		var pageTracker = _gat._getTracker("UA-12646352-2");
		pageTracker._trackPageview();
		} catch(err) {}</script>
   		<!-- FIN SCRIT GOOGLE ANALYTIC -->
	<body>
			<div id="en_tete"><!-- Attention ici on met la bannière dans le css et on y défini les propriété du lien -->
			
	<a href="index.php" title="Unis Vers Sel 11 - Accueil"><img src="img/headerwp.jpg" alt="Unis Vers Sel" title="Unis Vers Sel"/></a>
	<img src="img/bande-haut.png" alt="bande-haut" width="940px" height="70px"/>
