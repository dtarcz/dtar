<?php
			include("en-tete.php");
			?>
		</div>
 
		<div id="menu">
			<?php // Inclusion du menu:
			include("menu.php"); 
			?>			
		</div>
 
		<div id="corps">
			<h1>STATUTS</h1>
			
			<p><h2>ARTICLE 1:</h2> Il est fondé entre les adhérents aux présents statuts une association ayant pour titre : 
			<h3>« UNIS VERS SEL », loi de 1901</h3></p>
			<br />
			<p><h2>ARTICLE 2 : </h2>
				L'Association  se donne pour objectifs :
					<ul>
						<li>la constitution d'un réseau de membres désireux d'échanger entre eux des biens, des services ou des savoirs.</li>
						<li>l'organisation des échanges par la mise en relation des offres et des demandes.</li>
					</ul>
			</p>
			<br />
			<p><h2>ARTICLE 3 : </h2>
				Le siège social est fixé à Narbonne. La fixation de l'adresse exacte est de la compétence du Bureau.
			</p>
			<br />
			<p><h2>ARTICLE 4 : </h2>
				La durée de l'Association est illimitée.
			</p>
			<br />
			<p><h2>ARTICLE 5 : </h2>
				l'Association se compose de tous les membres ayant payé la cotisation annuelle, définie par l'AG. Il existe une adhésion individuelle et une adhésion couple-famille (qui compte pour deux adhérents).</p>
			<br />
			<p><h2>ARTICLE 6 : </h2>
				La qualité de membre se perd :</p>
					<ul>
						<li><p>par décès,</p></li>
						<li><p>par démission adressée par écrit au Secrétaire de l'Association,</p>
						</li>
						<li><p>par exclusion prononcée par le bureau pour une infraction aux présents statuts ou pour motif grave portant préjudice moral ou matériel à l'Association,</p>
						</li>
						<li><p>pour non-paiement de la cotisation.</p></li>
					</ul>
				<p>Avant la prise de décision éventuelle d'exclusion, le membre concerné est invité à fournir des explications au Bureau. Pour une décision d'exclusion, le quorum est de 75% et la majorité qualifiée des 2/3.</p>
			<br />
			<p><h2>ARTICLE 7 : </h2>
				L'Association est administrée par un Bureau comprenant au maximum 12 membres, élus pour 1 an par l'Assemblée Générale Ordinaire. Chaque membre est chargé d'un travail précis (voir charte d'adhésion). En cas de vacances, le Bureau pourvoit provisoirement au remplacement de ses membres. En cas de démission collective d'au moins 40% des membres du Bureau, l'Assemblée Générale Ordinaire doit impérativement être convoquée dans le mois suivant pour élire un nouveau Bureau dans sa totalité. Les membres sortants sont rééligibles.</p><p class="P2">
			<br />
			<p><h2>ARTICLE 8 : </h2>
				Le Bureau se réunit au moins 6 fois par  an et chaque fois qu'il est convoqué par écrit par le Secrétaire, ou sur la demande d'au moins le tiers de ses membres. Tout membre de l'Association peut assister aux réunions de Bureau. Les délibérations sont prises à la majorité des membres présents. Toutes les décisions du Bureau sont consignées dans un registre et signées par le secrétaire et le trésorier.
				Le Bureau est investi, de manière générale, des pouvoirs les plus étendus dans la limite des buts de l'Association et dans le cadre des résolutions adoptées par les Assemblées Générales.</p>
			<br />
			<p><h2>ARTICLE  9 : Assemblée Générale Ordinaire.</h2></p>
			<p>Elle comprend tous les membres de l'Association. Elle se tient au minimum 1 fois par an sur convocation du Secrétaire. Les convocations doivent être envoyées 15 jours avant la date de l'Assemblée et comporter l'ordre du jour. Toute proposition, signée par 3 membres, à jour de cotisation, et envoyée 1 mois avant l'AG, est inscrite de droit dans l'ordre du jour. L'Assemblée vote les différents rapports (activité, financier) et délibère sur toutes les autres questions à l'ordre du jour. Elle renouvelle les membres du Bureau, fixe le montant des cotisations (en euros) et le montant des soldes positifs ou négatifs « d'heure » à ne pas dépasser sur la feuille annuelle d'échanges.</p>
			<p>Les décisions sont prises à la majorité des membres présents et représentés, à jour de leur cotisation.</p>
			<p>Chaque adhérent ne peut être porteur que d'un seul mandat. Les décisions sont prises à main levée, sauf si un vote à bulletin secret est demandé par 3 personnes. Tous les votes nominatifs ont lieu à bulletins secrets. La convocation d'une AGO peut-être demandée par 25% des adhérents ou 40% du bureau sur ordre du jour précis. Elle doit alors être convoquée sous 1 mois.</p>
			<br />
			<p><h2>ARTICLE 10 : Assemblée Générale Extraordinaire. </h2></p>
			<p>L'AGExtraordinaire est convoquée et se déroule dans les mêmes conditions que l'AG Ordinaire. Toutefois, pour délibérer valablement, l'AGE doit comprendre au moins la moitié des membres (présents ou représentés). Si ce nombre n'est pas atteint, elle est à nouveau convoquée mais à 15 jours d'intervalle. Elle peut alors délibérer quel que soit le nombre des adhérents présents.</p>
			<p>Elle statue sur les questions de sa seule compétence, à savoir les modifications de statuts et la dissolution. Les délibérations sont prises à la majorité des 2/3 des membres présents et représentés.</p>
			<br />
			<p><h2>ARTICLE 11:</h2>
			 La dissolution est prononcée par une Assemblée Extraordinaire convoquée à cet effet qui désigne un ou plusieurs liquidateurs. L'actif net subsistant sera attribué à une ou plusieurs associations poursuivant des buts similaires, qui seront désignées par l'Assemblée Extraordinaire.</p>
			<br />
			<p><h2>ARTICLE 12:</h2>
			La charte d'adhésion est votée par L'Assemblée Générale. Elle est destinée à fixer les divers points non prévus par les présents statuts, notamment ceux qui ont trait au fonctionnement pratique de l'Association.</p>
	</body>
			</div>
 
		<div id="pied_de_page">
			<?php // Inclusion du pied de page:
			include("pied.php"); 
			?>
		</div>
